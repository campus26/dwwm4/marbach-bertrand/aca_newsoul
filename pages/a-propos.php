<?php
include("init/config-pages.php");
include("../assets/functions/functionsDB.php");

$title_page = $entity . " - À propos";

$breadcrumbs = array(
    array("url" => "../pages/index-landing-page.php", "title" => "Accueil"),
);

keepURL($_SERVER['REQUEST_URI']);
include("templates/header.php");
include("sections/about.php");
include("sections/strengths.php");
include("sections/partners.php");
include("templates/footer.php");
include("templates/include_js_scripts.php");
?>