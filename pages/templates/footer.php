        </div>
        <!-- Begin footer -->
        <footer class="footer footer-minimal">
            <div class="footer-main">
                <div class="container">
                    <div class="row items align-items-center">
                        <div class="col-lg-3 col-md-4 col-12 item">
                            <div class="widget-brand-info">
                                <div class="widget-brand-info-main">
                                    <a href="../pages/index-landing-page.php" class="logo" title="PathSoft">
                                        <img data-src="../assets/img/ACA2403Logo-ico.png" class="lazy" width="36" height="auto" src="../assets/img/ACA2403Logo-ico.png" alt="PathSoft" data-loaded="true" style="opacity: 1;">
                                        <span style="padding-left: 1.5rem; padding-top: 0.5rem;">Aéroclub de l'Ardèche</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md item">
                            <div class="footer-item">
                                <nav class="footer-nav">
                                    <ul class="footer-mnu footer-mnu-line">
                                        <li><a href="../pages/index-landing-page.php" class="hover-link" data-title="Accueil"><span>Accueil</span></a></li>
                                        <li><a href="../pages/a-propos.php" class="hover-link" data-title="À propos"><span>À
                                                    propos</span></a></li>
                                        <li><a href="../pages/dto.php" class="hover-link" data-title="DTO"><span>DTO</span></a>
                                        </li>
                                        <li><a href="../pages/flotte.php" class="hover-link" data-title="Flotte"><span>Flotte</span></a></li>
                                        <li><a href="../pages/ecole.php" class="hover-link" data-title="École"><span>École</span></a></li>
                                        <li><a href="../pages/prepa-vol-et-nav.php" class="hover-link" data-title="Prépa Vol & Nav"><span>Prépa Vol & Nav</span></a></li>
                                        <li><a href="../pages/contacts.php" class="hover-link" data-title="Contacts"><span>Contacts</span></a></li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer-bottom">
                <div class="container">
                    <div class="row justify-content-between items">
                        <div class="col-md-auto col-12 item">
                            <nav class="footer-links">
                                <ul>
                                    <li><a href="../pages/infos-legales.php">Informations légales</a></li>
                                    <li><a href="../pages/infos-adherents.php">Informations adhérents</a></li>
                                </ul>
                            </nav>
                        </div>
                        <div class="col-md-auto col-12 item">
                            <nav class="footer-info">
                                <ul>
                                    <li>
                                        <a href="https://www.facebook.com/profile.php?id=100064412122876" target="_blank"> Nous sommes là :
                                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512" width="24" height="24">
                                                <path fill="currentColor" d="M279.14 288l14.22-92.66h-88.91v-60.13c0-25.35 12.42-50.06 52.24-50.06h40.42V6.26S260.43 0 225.36 0c-73.22 0-121.08 44.38-121.08 124.72v70.62H22.89V288h81.39v224h100.17V288z" />
                                            </svg>
                                        </a>
                                        <a href="https://www.instagram.com/aeroclubdelardeche?igsh=MTNjaWM1d21oZ2IwdA==" target="_blank"> et aussi ici :
                                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" width="24" height="24">
                                                <path fill="currentColor" d="M224.1 141c-63.6 0-114.9 51.3-114.9 114.9s51.3 114.9 114.9 114.9S339 319.5 339 255.9 287.7 141 224.1 141zm0 189.6c-41.1 0-74.7-33.5-74.7-74.7s33.5-74.7 74.7-74.7 74.7 33.5 74.7 74.7-33.6 74.7-74.7 74.7zm146.4-194.3c0 14.9-12 26.8-26.8 26.8-14.9 0-26.8-12-26.8-26.8s12-26.8 26.8-26.8 26.8 12 26.8 26.8zm76.1 27.2c-1.7-35.9-9.9-67.7-36.2-93.9-26.2-26.2-58-34.4-93.9-36.2-37-2.1-147.9-2.1-184.9 0-35.8 1.7-67.6 9.9-93.9 36.1s-34.4 58-36.2 93.9c-2.1 37-2.1 147.9 0 184.9 1.7 35.9 9.9 67.7 36.2 93.9s58 34.4 93.9 36.2c37 2.1 147.9 2.1 184.9 0 35.9-1.7 67.7-9.9 93.9-36.2 26.2-26.2 34.4-58 36.2-93.9 2.1-37 2.1-147.8 0-184.8zM398.8 388c-7.8 19.6-22.9 34.7-42.6 42.6-29.5 11.7-99.5 9-132.1 9s-102.7 2.6-132.1-9c-19.6-7.8-34.7-22.9-42.6-42.6-11.7-29.5-9-99.5-9-132.1s-2.6-102.7 9-132.1c7.8-19.6 22.9-34.7 42.6-42.6 29.5-11.7 99.5-9 132.1-9s102.7-2.6 132.1 9c19.6 7.8 34.7 22.9 42.6 42.6 11.7 29.5 9 99.5 9 132.1s2.7 102.7-9 132.1z" />
                                            </svg>
                                        </a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                        <div class="col-md-auto col-12 item">
                            <div class="copyright">© 2024 ACA - Aéroclub de l'Ardèche. Tous droits réservés.</div>
                        </div>
                    </div>
                </div>
            </div>
        </footer><!-- End footer -->

        </main><!-- End main -->