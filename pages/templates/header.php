<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">

    <title><?php echo $title_page; ?></title>

    <meta name="description" content="Description">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=2">

    <link rel="icon" href="../assets/img/favicon/favicon.ico" type="image/x-icon">

    <link rel="stylesheet" href="../assets/css/bootstrap-grid.css">
    <link rel="stylesheet" href="../assets/css/style.css">
    <link rel="preload" href="../assets/fonts/source-sans-pro-v21-latin/source-sans-pro-v21-latin-regular.woff2" as="font" type="font/woff2" crossorigin>
    <link rel="preload" href="../assets/fonts/source-sans-pro-v21-latin/source-sans-pro-v21-latin-700.woff2" as="font" type="font/woff2" crossorigin>
    <link rel="preload" href="../assets/fonts/montserrat-v25-latin/montserrat-v25-latin-700.woff2" as="font" type="font/woff2" crossorigin>
    <link rel="preload" href="../assets/fonts/montserrat-v25-latin/montserrat-v25-latin-600.woff2" as="font" type="font/woff2" crossorigin>

    <link rel="preload" href="../assets/fonts/material-icons/material-icons.woff2" as="font" type="font/woff2" crossorigin>
    <link rel="preload" href="../assets/fonts/material-icons/material-icons-outlined.woff2" as="font" type="font/woff2" crossorigin>

</head>

<body>

    <main class="main">
        <div class="main-inner">

            <!-- Begin mobile main menu  -->
            <nav class="mmm">
                <div class="mmm-content">
                    <ul class="mmm-list">
                        <li>
                            <a href="../pages/index-landing-page.php">Accueil</a>
                        </li>
                        <li>
                            <a href="../pages/a-propos.php">À propos</a>
                        </li>
                        <li>
                            <a href="../pages/dto.php">DTO</a>
                        </li>
                        <li>
                            <a href="../pages/flotte.php">Flotte</a>
                        </li>
                        <li>
                            <a href="../pages/ecole.php">École</a>
                        </li>
                        <li>
                            <a href="../pages/prepa-vol-et-nav.php">Prépa Vol & Nav</a>
                        </li>
                        <li>
                            <a href="../pages/contacts.php">Contacts</a>
                        </li>
                        <li>
                            <a href="https://gdv.assapan.fr/meteo/webcam3.dll/Webcam?path=aca/manche" target="_blank" ><img src="https://gdv.assapan.fr/meteo/webcam3.dll/Webcam?path=aca/manche" height="36px" style="vertical-align: middle;" alt="Webcam Manche">  WebCam N</a>
                        </li>
                        <li>
                            <a href="https://gdv.assapan.fr/meteo/webcam3.dll/Webcam?path=aca/piste" target="_blank" ><img src="https://gdv.assapan.fr/meteo/webcam3.dll/Webcam?path=aca/piste" height="36px" style="vertical-align: middle;" alt="Webcam Sampzon">  WebCam S</a>
                        </li>
                        <li>
                        <a href="../pages/index-onboarding.php">
                        <img src="<?php echo $profilePictureUrl; ?>" width="36" height="36" style="vertical-align: middle;" alt="Connexion"><span><?php echo $userNameFirst . " " . $userNameLast; ?></span></a>
                        </li>
                                                <?php
                                                    if ($grinchLevel == 0) {
                                                ?>
                                                <li>
                                                <a href="../pages/admin.php">Admin</a>
                                                </li>
                                                <?php
                                                    }
                                                ?>                        
                    </ul>
                </div>
            </nav><!-- End mobile main menu -->

            <header class="header header-minimal">
                <nav class="header-fixed">
                    <div class="container">
                        <div class="row flex-nowrap align-items-center justify-content-between">
                        <div class="col-auto header-fixed-col logo-wrapper">
                            <a href="../pages/index-landing-page.php" class="logo" title="PathSoft">
                                <img src="../assets/img/ACA2403Logo-ico.png" max-width="36" max-height="36" width="auto" height="auto" alt="PathSoft">
                            </a>
                            <a href="https://gdv.assapan.fr/meteo/gdvisapi.dll/aca" title="réservation HdV" style="padding-left: 1.5rem; padding-top: 0.3rem;"> réservation HdV</a>
                            </div>
                            <div class="col-auto col-xl col-static header-fixed-col d-none d-xl-block">
                                <div class="row flex-nowrap align-items-center justify-content-end">
                                    <div class="col header-fixed-col d-none d-xl-block col-static">
                                        <!-- Begin main menu -->
                                        <nav class="main-mnu">
                                            <ul class="main-mnu-list">
                                                <li>
                                                    <a href="../pages/index-landing-page.php" data-title="Accueil">
                                                        <span>Accueil</span></a>
                                                </li>

                                                <li>
                                                    <a href="../pages/a-propos.php" data-title="À propos">
                                                        <span>À propos</span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="../pages/dto.php" data-title="DTO">
                                                        <span>DTO</span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="../pages/flotte.php" data-title="Flotte">
                                                        <span>Flotte</span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="../pages/ecole.php" data-title="École">
                                                        <span>École</span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="../pages/prepa-vol-et-nav.php" data-title="Prépa Vol & Nav">
                                                        <span>Prépa Vol & Nav</span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="../pages/contacts.php" data-title="Contacts">
                                                        <span>Contacts</span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="https://gdv.assapan.fr/meteo/webcam3.dll/Webcam?path=aca/manche" target="_blank" data-title=" < Nord">
                                                        <img src="https://gdv.assapan.fr/meteo/webcam3.dll/Webcam?path=aca/manche" height="36px" alt="Webcam Manche">
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="https://gdv.assapan.fr/meteo/webcam3.dll/Webcam?path=aca/piste" target="_blank" data-title=" Sud > ">
                                                        <img src="https://gdv.assapan.fr/meteo/webcam3.dll/Webcam?path=aca/piste" height="36px" alt="Webcam Sampzon">
                                                    </a>
                                                </li>
                                                
                                                <li>
                                                    <a href="../pages/index-onboarding.php" title="<?php echo $userNameFirst . " " . $userNameLast; ?>">
                                                    <img src="<?php echo $profilePictureUrl; ?>" width="48" height="48" width="auto" height="auto" alt="user login">
                                                    </a>                                            
                                                </li>
                                                
                                                <?php
                                                    if ($grinchLevel == 0) {
                                                ?>
                                                <li>
                                                <a href="../pages/admin.php" data-title="Admin">
                                                        <span>Admin</span>
                                                    </a>
                                                    </li>
                                                <?php
                                                    }
                                                ?>                                                
                                            </ul>
                                        </nav><!-- End main menu -->
                                    </div>
                                </div>
                            </div>
                            <div class="col-auto d-block d-xl-none header-fixed-col">
                                <div class="main-mnu-btn">
                                    <span class="bar bar-1"></span>
                                    <span class="bar bar-2"></span>
                                    <span class="bar bar-3"></span>
                                    <span class="bar bar-4"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </nav>
            </header>

            <!-- Begin bread crumbs -->
            <?php
            if (!empty($breadcrumbs)) {
            ?>
            <nav class="bread-crumbs">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <ul class="bread-crumbs-list">
                                <?php foreach ($breadcrumbs as $breadcrumb): ?>
                                    <li>
                                        <a href="<?php echo $breadcrumb["url"]; ?>"><?php echo $breadcrumb["title"]; ?></a>
                                        <i class="material-icons md-18">chevron_right</i>
                                    </li>                        
                                <?php endforeach; ?>
                                    <li><?php echo $title_page; ?></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </nav>
            <?php
            }
            ?>
            <!-- End bread crumbs -->
    