<form action="#!" method="post" id="UserMessageDetails" class="form-submission contact-form contact-form-padding" novalidate>
                                <input type="hidden" name="access_key" value="054ec81a-6ee7-48fa-a029-db1effb38a13">
                                <input type="hidden" name="Subject" value="Contact form">
                                <div class="row gutters-default">
                                    <div class="col-12">
                                        <h3>Formulaire de Contact</h3>
                                    </div>
                                    <div class="col-xl-4 col-sm-6 col-12">
                                        <div class="form-field">
                                            <label for="contact-name" class="form-field-label">Nom/Full Name</label>
                                            <input type="text" class="form-field-input" name="Name" value="" autocomplete="off" id="contact-name" required data-pristine-required-message="champ requis.">
                                        </div>
                                    </div>
                                    <div class="col-xl-4 col-sm-6 col-12">
                                        <div class="form-field">
                                            <label for="contact-phone" class="form-field-label">Tel. Mobile/Phone
                                                Number</label>
                                            <input type="tel" class="form-field-input mask-phone" name="Phone" value="" autocomplete="off" id="contact-phone" required data-pristine-required-message="champ requis.">
                                        </div>
                                    </div>
                                    <div class="col-xl-4 col-12">
                                        <div class="form-field">
                                            <label for="contact-email" class="form-field-label">Email</label>
                                            <input type="email" class="form-field-input" name="Email" value="" autocomplete="off" id="contact-email" required data-pristine-required-message="champ requis." data-pristine-email-message="entrez ue adresse valide">
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-field">
                                            <label for="contact-message" class="form-field-label">Message</label>
                                            <textarea type="text" class="form-field-input" name="Message" id="contact-message" required cols="30" rows="6">
                                            </textarea>
                                        </div>
                                    </div>
                                    <div class="h-captcha" data-captcha="true"></div>
                                    <div class="form-btn">
                                        <button type="submit" class="btn btn-w240 ripple"><span>On vous répondra 😀
                                                / send your message</span></button>
                                    </div>
                                    <div class="form-btn" id="result"></div>
                                </div>
                            </form>