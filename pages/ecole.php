<?php
include("init/config-pages.php");
include("../assets/functions/functionsDB.php");

$title_page = $entity . " - l'École de pilotage";

$breadcrumbs = array(
    array("url" => "../pages/index-landing-page.php", "title" => "Accueil"),
);

keepURL($_SERVER['REQUEST_URI']);
include("templates/header.php");
include("sections/flying_school_page.php");
include("templates/footer.php");
?>

<script src="../assets/js/playvideo.js"></script>   
  
<?php
include("templates/include_js_scripts.php");
?>