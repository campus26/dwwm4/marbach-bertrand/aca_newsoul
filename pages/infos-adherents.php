<?php
include("init/config-pages.php");
include("../assets/functions/functionsDB.php");

$title_page = $entity . " - Informations adhérents";

$breadcrumbs = array(
    array("url" => "../pages/index-landing-page.php", "title" => "Accueil"),
);

keepURL($_SERVER['REQUEST_URI']);
include("templates/header.php");
include("sections/members_gen_info_page.php");
if ($grinchLevel <= 1400) {
    include("sections/members_committee.php");
    include("sections/members_directors_board.php");
}
if ($grinchLevel <= 353 && $grinchLevel != 1013) {
    include("sections/members_mom.php");
}
include("templates/footer.php");
include("templates/include_js_scripts.php");
?>