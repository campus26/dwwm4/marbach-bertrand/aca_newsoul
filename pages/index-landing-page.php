<?php
include("init/config-pages.php");
include("../assets/functions/functionsDB.php");


$title_page = $entity . " Aéroclub de l'Ardèche - En vol depuis plus de 70 ans !...";

$breadcrumbs = array();

keepURL($_SERVER['REQUEST_URI']);
include("templates/header.php");
include("sections/intro.php");
include("sections/application_invitation.php");
include("sections/strengths.php");
include("sections/counters.php");
include("sections/customer_testimonials.php");
include("sections/airman_tales.php");
include("templates/footer.php");
include("templates/include_js_scripts.php");
