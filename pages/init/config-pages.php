<?php
require("../assets/functions/functions.php");

date_default_timezone_set('UTC');
session_start();

$userID = $_SESSION['userID'];
$entity = $_SESSION['entity'];
$userNameFirst = $_SESSION['userNameFirst'] ;
$userNameLast = $_SESSION['userNameLast'] ;
$extStoragePath = $_SESSION['extStoragePath'];
$grinchLevel = $_SESSION['grinchLevel'];
$sessionTimeOut = $_SESSION['sessionTimeOut'];
$profilePictureUrl = $_SESSION['profilePictureUrl'];
$timeStartActualSession = $_SESSION['timeStartActualSession'];

$timeNow = date('Y-m-d H:i:s');


session_write_close();


$deprecatedSessionFlag = isSessionDeprecated($timeStartActualSession, $timeNow, $sessionTimeOut);


session_start();

if ($deprecatedSessionFlag) {
    $_SESSION['userID'] = "";
    $_SESSION['userNameFirst'] = "";
    $_SESSION['userNameLast'] = "🛫 ouvrez votre session 😊";
    $_SESSION['extStoragePath'] = $extStoragePath = "";
    $_SESSION['grinchLevel'] = $grinchLevel = 7600;
    $_SESSION['sessionTimeOut'] = $sessionTimeOut = -1;
    $_SESSION['profilePictureUrl'] = $profilePictureUrl = "../assets/img/icons/statusIsNotConnected.png"; 
}
session_write_close();
?>