<?php
require("assets/functions/functions.php");
date_default_timezone_set('UTC');

//grinch : Golden Rigorous Internal Network Clearance Hierarchy 
// G: Golden            (level 0 :    0) • sessionTimeOut :    no • *** utilsé pour le développement ***)
// R: Reserved          (level 1 :  353) • sessionTimeOut : 1800s • Président,Secrétaire, Trésorier, Mécanicien, SGS, RP)
// I: Illustrious       (level 2 : 1013) • sessionTimeOut : 1800s • externe, DSAC)
// N: Notable access    (level 3 : 1031) • sessionTimeOut : 1800s • membres du comité directeur, instructeurs)
// C: Conventional      (level 4 : 1400) • sessionTimeOut : 1800s • adhérents de l'association sans fonctions électives)
// H: Humble access     (level 5 : 7600) • sessionTimeOut : 1800s • visiteurs, non adhérents sans accès spécifique)

if (isset($_GET['grinchLevel']) && isset($_GET['profilePictureUrl']) && isset($_GET['timeStartActualSession'])) {
    $userID = $_GET['userID'];
    $entity = $_GET['entity'];
    $userNameFirst= $_GET['userNameFirst'];
    $userNameLast= $_GET['userNameLast'];
    $extStoragePath = $_GET['extStoragePath'];
    $grinchLevel = setUserPrivilege($_GET['grinchLevel']);
    $sessionTimeOut = $_GET['sessionTimeOut'];
    $profilePictureUrl = $_GET['profilePictureUrl'];
    $timeStartActualSession = $_GET['timeStartActualSession'];
} else {
    $userID = "";
    $entity = "ACA";
    $userNameFirst = "";
    $userNameLast = "🛫 ouvrez votre session 😊";
    $extStoragePath = "";
    $grinchLevel = setUserPrivilege("H");
    $sessionTimeOut = -1; 
    $profilePictureUrl = "../assets/img/icons/statusIsNotConnected.png";
    $timeStartActualSession  = date('Y-m-d H:i:s');
}
$timeNow = date ('Y-m-d H:i:s');
// $deprecatedSessionFlag = isSessionDeprecated($timeStartActualSession, $timeNow, $sessionTimeOut);

session_start();
$_SESSION['userID'] = $userID;
$_SESSION['entity'] = $entity;
$_SESSION['userNameFirst'] = $userNameFirst;
$_SESSION['userNameLast'] = $userNameLast;
$_SESSION['extStoragePath'] = $extStoragePath;
$_SESSION['grinchLevel'] = $grinchLevel;
$_SESSION['sessionTimeOut'] = $sessionTimeOut;
$_SESSION['profilePictureUrl'] = $profilePictureUrl;
$_SESSION['timeStartActualSession'] = $timeStartActualSession;
$_SESSION['timeNow'] = $timeNow;
session_write_close();

// $_SESSION['deprecatedSessionFlag'] = $deprecatedSessionFlag;

// if ($deprecatedSessionFlag) {
//     $_SESSION['userNameFirst'] = "";
//     $_SESSION['userNameLast'] = "Veuillez vous connecter";
//     $_SESSION['externalStorage'] = $extStoragePath = "";
//     $_SESSION['grinchLevel'] = $grinchLevel = 7600;
//     $_SESSION['sessionTimeOut'] = -1;
//     $_SESSION['profilePictureUrl'] = $profilePictureUrl = "../assets/img/icons/statusIsNotConnected.png";

// }

// Define a class to manage session variables
// class SessionManager {
//     public function __construct() {
//         // Start the session
//         session_start();
//     }

//     public function setSessionVariable($name, $value) {
//         $_SESSION[$name] = $value;
//     }

//     public function getSessionVariable($name) {
//         return isset($_SESSION[$name]) ? $_SESSION[$name] : null;
//     }
// }

// Instantiate the SessionManager object
// $sessionManager = new SessionManager();

// Set session variables
// $sessionManager->setSessionVariable('entity', 'Your Entity');
// $sessionManager->setSessionVariable('extStoragePath', 'External Storage');
// $sessionManager->setSessionVariable('grinchLevel', 'Grinch Level');
// $sessionManager->setSessionVariable('profilePictureUrl', 'Profile Picture URL');
// $sessionManager->setSessionVariable('startTimeStamp', 'Start Time Stamp');
// $sessionManager->setSessionVariable('actualTimeStamp', 'Actual Time Stamp');
// $sessionManager->setSessionVariable('sessionTimeOut', 'Session Time Out');
// $sessionManager->setSessionVariable('deprecatedSessionFlag', 'Deprecated Session Flag');

// Retrieve session variables
// $entity = $sessionManager->getSessionVariable('entity');
// $extStoragePath = $sessionManager->getSessionVariable('extStoragePath');
// $grinchLevel = $sessionManager->getSessionVariable('grinchLevel');
// $profilePictureUrl = $sessionManager->getSessionVariable('profilePictureUrl');
// $startTimeStamp = $sessionManager->getSessionVariable('startTimeStamp');
// $actualTimeStamp = $sessionManager->getSessionVariable('actualTimeStamp');
// $sessionTimeOut = $sessionManager->getSessionVariable('sessionTimeOut');
// $deprecatedSessionFlag = $sessionManager->getSessionVariable('deprecatedSessionFlag');

// Use the retrieved session variables as needed
?>