<?php
include("init/config-pages.php");
include("../assets/functions/functionsDB.php");

$title_page = $entity . " - Contact";

$breadcrumbs = array(
    array("url" => "../pages/index-landing-page.php", "title" => "Accueil"),
);

keepURL($_SERVER['REQUEST_URI']);
include("templates/header.php");
include("sections/contact_page.php");
include("sections/google_map.php");
include("templates/footer.php");
include("templates/include_js_scripts.php");
?>