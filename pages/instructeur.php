<?php
include("init/config-pages.php");
include("../assets/functions/functionsDB.php");

$title_page = $entity . " - le coin des FI";

$breadcrumbs = array(
    array("url" => "../pages/index-landing-page.php", "title" => "Accueil"),
    array("url" => "../pages/ecole.php", "title" => "École"),
);

keepURL($_SERVER['REQUEST_URI']);
include("templates/header.php");
include("sections/flying_school_instructor.php");
include("templates/footer.php");
include("templates/include_js_scripts.php");
?>    