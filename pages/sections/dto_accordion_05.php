<li class="accordion-item section-bgc">
									<div class="accordion-trigger">
										<div><a href="#!" target="_blank" title="" style="display: inline-block;">
												<img src="../assets/img/ACA2403-IconeNB-ruban-medaille.png"
													alt="icone d'incident et d'alerte'" width="44" height="44">
											</a>∾ Vols de découverte
										</div>
									</div>
									<div class="accordion">
										<div class="row gutters-default">
											<div class="accordion-content content">
												<p><a href="<?php echo $extStoragePath; ?>aca-pdf/ACA2403-DTO-VOLS-DE-DECOUVERTE-DGAC-vols-initiation-BIA-20120702.pdf"
														target="_blank">
														<span><i
																class="material-icons material-icons-outlined md-24">download_for_offline</i></span></a>⇥ Circulaire DGAC à propos des BIA et des vols d'initiation
																<?php if ($grinchLevel <= 1400) { ?><br>
																<a
															href="<?php echo $extStoragePath; ?>aca-pdf/ACA2403-DTO-Vols-de-decouverte-v2_0_20230606.pdf"
															target="_blank">
															<span><i
																	class="material-icons material-icons-outlined md-24">download_for_offline</i></span></a>⇥ Vol
														de découverte (2023)<?php } ?>

												</p>