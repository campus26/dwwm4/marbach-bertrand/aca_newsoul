<section class="section section-without-padding-bottom">
				<div class="container">
					<div class="row items spincrement-container">
						<div class="col-xl-3 col-md-6 col-12 item">
							<div class="counter-min">
								<div class="counter-min-block">
									<div class="counter-min-ico">
										<i class="material-icons material-icons-outlined md-36">history</i>
									</div>
									<div class="counter-min-numb spincrement" data-from="0" data-to="70">0</div>
								</div>
								<div class="counter-min-info">
									<h4 class="counter-min-heading">années d'expérience</h4>
								</div>
							</div>
						</div>
						<div class="col-xl-3 col-md-6 col-12 item">
							<div class="counter-min">
								<div class="counter-min-block">
									<div class="counter-min-ico">
										<i class="material-icons material-icons-outlined md-36">history</i>
									</div>
									<div class="counter-min-numb spincrement" data-from="0" data-to="5">0</div>
								</div>
								<div class="counter-min-info">
									<h4 class="counter-min-heading">beaux avions</h4>
								</div>
							</div>
						</div>
						<div class="col-xl-3 col-md-6 col-12 item">
							<div class="counter-min">
								<div class="counter-min-block">
									<div class="counter-min-ico">
										<i class="material-icons material-icons-outlined md-36">chat</i>
									</div>
									<div class="counter-min-numb spincrement" data-from="0" data-to="2">0</div>
								</div>
								<div class="counter-min-info">
									<h4 class="counter-min-heading">Instructeurs</h4>
								</div>
							</div>
						</div>
						<div class="col-xl-3 col-md-6 col-12 item">
							<div class="counter-min">
								<div class="counter-min-block">
									<div class="counter-min-ico">
										<i class="material-icons material-icons-outlined md-36">build_circle</i>
									</div>
									<div class="counter-min-numb spincrement" data-from="0" data-to="1">0</div>
								</div>
								<div class="counter-min-info">
									<h4 class="counter-min-heading">mécanicien</h4>
								</div>
							</div>
						</div>
						<div class="col-xl-3 col-md-6 col-12 item">
							<div class="counter-min">
								<div class="counter-min-block">
									<div class="counter-min-ico">
										<i class="material-icons material-icons-outlined md-36">assignment_ind</i>
									</div>
									<div class="counter-min-numb spincrement" data-from="0" data-to="63">0</div>
								</div>
								<div class="counter-min-info">
									<h4 class="counter-min-heading">adhérents</h4>
								</div>
							</div>
						</div>
						<div class="col-xl-3 col-md-6 col-12 item">
							<div class="counter-min">
								<div class="counter-min-block">
									<div class="counter-min-ico">
										<i class="material-icons material-icons-outlined md-36">flight</i>
									</div>
									<div class="counter-min-numb spincrement" data-from="0" data-to="1200">0</div>
								</div>
								<div class="counter-min-info">
									<h4 class="counter-min-heading">h de vol par an</h4>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>