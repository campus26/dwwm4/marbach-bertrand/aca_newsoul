                                <div class="tabs-item content">
                                    <p>Pour obtenir la licence de pilote privé d'avion léger LAPL
                                        (<strong>L</strong>ight <strong>A</strong>ircraft
                                        <strong>P</strong>ilot <strong>L</strong>icence),
                                        afin d'être présenté à l'examen en vol, les conditions à réunir sont d'avoir :
                                    <h5>LAPL (EASA)</h5>
                                    <ul>
                                        <li>une visite médicale aéronautique classe 2 en état de validité</li>
                                        <li>réussi l'examen théorique depuis moins de deux ans le jour du test en vol
                                        </li>
                                        <li>17 ans révolus le jour du test pratique</li>
                                        <li>navigué en solo vers un aérodrome éloigné d'au minimum 40 milles nautiques
                                        </li>
                                        <li>effectué des vols 'solo' pour un minimum de 6 heures (tours de piste, vol
                                            local, navigation)</li>
                                        <li>effectué au minimum 15h de vol en double commande</li>
                                        <li>effectué une formation d'une durée totale d'au minimim 30h (vols avec
                                            instructeur plus
                                            vols en solo)</li>
                                        </li>
                                    </ul>
                                    <h5> Privilèges de la licence LAPL </h5>
                                    Ceux-ci sont de voler :
                                    <ul>
                                        <li>à vue, de jour, en monomoteur</li>
                                        <li>dans des pays francophones adhérents à l'EASA</li>
                                        <li>dans des avions d'une masse maximale au décollage de 2 tonnes</li>
                                        <li>dans des avions transportant au maximum 3 passagers</li>
                                        <li>dans le cadre d'un vol non commercial (partage des frais directs de vol
                                            possible)
                                        </li>
                                        <li> NB. Le transport de passagers ne sera possible, après réussite à l'examen
                                            en
                                            vol, qu'après avoir volé 10 heures seul à bord</li>
                                    </ul>
                                    <h5> Maintien d'exercice des privilèges de la LAPL </h5>
                                    <ul>
                                        <li>visite médicale aéronautique classe 2 en état de validité</li>
                                        <li>avoir effectué dans les 24 derniers mois au minimum 12h de vol (somme
                                            glissante) dont 6
                                            minimum comme commandant de bord
                                        </li>
                                        <li>avoir effectué, dans les trois derniers mois, trois atterrissages et trois
                                            décollages pour
                                            l'emport passager
                                        </li>
                                    </ul>
                                    <a href="<?php echo $extStoragePath; ?>aca-pdf/ACA2403-DTO-LAPL-Cepadues-190100_poster des Matrices.pdf"
                                        target="_blank">
                                        <span><i
                                                class="material-icons material-icons-outlined md-24">download_for_offline</i></span></a>
                                    ⇥ Le programme pratique suivi est le programme 'Cepadues', qui peut être téléchargé
                                    ici
                                    <br>
                                    <br>
                                    <mark>D'une part, il convient de souligner que le nombre total d'heures de vol avant
                                        d'être autorisé à transporter
                                        des passagers est de 40 heures minimum. D'autre part, il est important de noter
                                        que ce chiffre de 40 heures (30+10) représente le strict
                                        <strong>minimum</strong>
                                        imposé par la réglementation.</mark>
                                    <br><br><br>
                                    <span><i
                                            class="material-icons material-icons-outlined md-24">warning_amber</i></span>Le
                                    nombre d'heures
                                    de vol nécessaires à un élève pilote pour être
                                    présenté à l'examen pratique varie en fonction d'un nombre non
                                    exhaustifs de paramètres tels que :

                                    <ul>
                                        <li>capacités techniques et non techniques</li>
                                        <li>âge</li>
                                        <li>disponibilité :
                                            <ul>
                                                <li>temporelle</li>
                                                <li>financière</li>
                                                <li>instructeur</li>
                                                <li>avion (mécanique, maintenance)</li>
                                            </ul>
                                        </li>
                                        <li>assiduité</li>
                                        <li>météo</li>
                                        <li>etc</li>
                                    </ul>
                                    Il est couramment admis qu'un projet tel que celui de passer sa licence de
                                    pilote prend du temps et que deux, trois ans sont très
                                    fréquemment observés.
                                    <br><br>
                                    La <strong>progression</strong> se fait selon un programme déposé auprès de la DGAC et <strong>la
                                    formation</strong> est un <mark>vrai travail d'équipe</mark> entre l'équipe pédagogique (le ou les
                                    instructeurs) et l'élève pilote.
                                    </p>
                                </div>