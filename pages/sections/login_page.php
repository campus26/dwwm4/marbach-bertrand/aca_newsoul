<!DOCTYPE html>
<!-- Coding By CodingNepal - www.codingnepalweb.com -->
<!-- Customised by Bertrand MARBACH +33(6) 519 777 46 -->

<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $title_page; ?></title>
    <link rel="preload" href="../assets/fonts/source-sans-pro-v21-latin/source-sans-pro-v21-latin-regular.woff2" as="font" type="font/woff2" crossorigin>
    <link rel="preload" href="../assets/fonts/source-sans-pro-v21-latin/source-sans-pro-v21-latin-700.woff2" as="font" type="font/woff2" crossorigin>

    <link rel="stylesheet" href="../assets/css/login-style.css">
    <script src="../assets/js/setLocalStorage.js" ></script>
    <script src="../assets/js/login-script.js" defer></script>
</head>
<body>
    <header>
        <nav class="navbar">
            <span class="hamburger-btn">menu</span>
            <a href="#" class="logo">
                <img src="../assets/img/ACA2403Logo-ico.png" alt="logo">
                <h2>Aéroclub de l'Ardèche</h2>
            </a>
            <ul class="links">
                <span class="close-btn">close</span>
                <li><a href="#">Accueil</a></li>
                <li><a href="#">À propos</a></li>
                <li><a href="#">DTO</a></li>
                <li><a href="#">Flotte</a></li>
                <li><a href="#">École</a></li>
                <li><a href="#">Prépa Vol & Nav</a></li>
                <li><a href="#">Contact</a></li>
                <li><a href="https://gdv.assapan.fr/meteo/webcam3.dll/Webcam?path=aca/manche" target="_blank" ><img src="https://gdv.assapan.fr/meteo/webcam3.dll/Webcam?path=aca/manche" height="36px" style="vertical-align: middle;" alt="Webcam Manche"></a></li>
                <li><a href="https://gdv.assapan.fr/meteo/webcam3.dll/Webcam?path=aca/piste" target="_blank" ><img src="https://gdv.assapan.fr/meteo/webcam3.dll/Webcam?path=aca/piste" height="36px" style="vertical-align: middle;" alt="Webcam Sampzon"></a></li>
            </ul>
            <button class="login-btn"><img src="../assets/img/icons/statusIsNotConnected.png" width="36" height="36" style="vertical-align: middle;" alt="Connexion"></a></button>
        </nav>
    </header>
    <div class="blur-bg-overlay"></div>
    <div class="form-popup">    
        <span class="close-btn">fermer</span>
        <!-- from here  -->
        <div class="form-box login" id="login">
            <div class="form-details">
                <h2>Bonjour !</h2>
                <p>Merci de saisir vos identifiants club </p>
            </div>
            <div class="form-content">
                <h2>IDENTIFICATION</h2>
                <form action="../assets/functions/validate_credentials.php" method="POST">
                    <div class="input-field email-input-field">
                        <input type="email" required>
                        <label>Votre email est votre identifiant</label>
                    </div>
                    <div class="input-field password-input-field">
                        <input type="password" required>
                        <label>mot de passe</label>
                    </div>
                    <a href="../pages/contacts.php" class="forgot-pass-link"></a>
                    <button class="login-input-btn" type="submit">Accéder</button>
                </form>
                <div class="bottom-link">   
                    Pas d'identifiant/de mot de passe ?
                    <a href="#" id="signup-link" data-target="signup">s'enregistrer</a>
                </div>
            </div>
        </div>
        <!--to there -->
        <!-- from here -->
        <div class="form-box signup" id="signup">
            <div class="form-details">
                <h2>Activer mon compte</h2>
                <p>membre de l'Aéroclub : activez ici votre accès reservé</p>
            </div>
            <div class="form-content">
                <h2>S'ENREGISTRER</h2>
                <form action="#">
                    <div class="input-field">
                        <input type="text" required autocomplete= "off">
                        <label>saisir ici l'email</label>
                    </div>
                    <div class="input-field">
                        <input type="password" required autocomplete= "off">
                        <label>créez votre mot de passe</label>
                    </div>
                    <div class="policy-text">
                        <input type="checkbox" id="policy-member">
                        <label for="policy">
                            Je confirme avoir lu et accepté le '
                            <a href="#" class="option">règlement intérieur</a> '
                        </label>
                    </div>
                    <button type="submit">S'enregistrer</button>
                </form>
                <div class="bottom-link">
                    Visiteur ? 
                    <a href="#" id="Visitor-link" data-target="visitor">obtenir votre enregistrement</a>
                </div>
            </div>
        </div>
        <!--to there -->
        <!-- from here  -->
        <div class="form-box visitor" id="visitor"> 
            <div class="form-details">
                <h2>Visiteur</h2>
                <p>Vous serez reconnu !</p>
            </div>
            <div class="form-content">
                <h2>RESTER INVITÉ</h2>
                <form action="#">
                    <div class="input-field">
                        <input type="text" required autocomplete= "off">
                        <label>saisir ici un email</label>
                    </div>
                    <div class="input-field">
                        <input type="password" required autocomplete= "off">
                        <label>et un mot de passe</label>
                    </div>
                    <div class="policy-text">
                        <input type="checkbox" id="policy-visitor">
                        <label for="policy">
                            J'ai compris que je n'aurai pas accès à l'intégralité du site, 
                            ni à certains des fichiers proposés au téléchargements aux adhérents
                        </label>
                    </div>
                    <button type="submit">Rejoindre</button>
                </form>
                <div class="bottom-link">
                    Déjà identifié? 
                    <a href="#" id="login-link" data-target="login">Accéder</a>
                </div>
            </div>
        </div>
        <!--to there -->
    </div>
</body>
</html>