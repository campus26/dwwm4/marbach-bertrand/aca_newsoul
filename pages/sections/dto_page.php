<div class="section">
				<div class="container">
					<div class="row">
						<div class="col-12">
							<div class="section-heading heading-center">

								<h1><img src="../assets/img/ACA2403-Logo-EASA.png" alt="icône EASA" height="60"><img
										src="../assets/img/ACA2403-Logo-DGAC.png" alt="icône DGAC" height="60"> DTO <img
										src="../assets/img/ACA2403Logo-ico.png" alt="icône DGAC" height="60">
								</h1>
								<div class="section-subheading">La partie école de l'Aéroclub de l'Ardèche est déclarée
									auprès de la DGAC sous le N°FR.DTO.0340</div>

							</div>
						</div>
						<div class="col-12">
							<div class="col-12 item">
								<p>Le DTO (organisme de formation déclaré, <strong>D</strong>eclared
									<strong>T</strong>raining
									<strong>O</strong>rganisation) est l'appellation
									administrative
									d'une école de pilotage dans l'Union Européenne. La continuité de la traçabilité est
									consignée dans cette partie

								<h5>Quelques ressources documentaires pour commencer, puis le manuel de notre DTO</h5>
								<a href="<?php echo $extStoragePath; ?>aca-pdf/ACA2403-DTO•votre-aeroclub-devient-dto.original.pdf"
														target="_blank">
														<span><i
																class="material-icons material-icons-outlined md-24">download_for_offline</i></span></a>⇥ l'aéroclub : un DTO<br>
															
								<a href="<?php echo $extStoragePath; ?>aca-pdf/ACA2403-DTO-Guide-Exploitation-aeronefs-Partie-NCO-Ed1V0-8_12_2021.pdf"
																target="_blank">
																<span><i
																		class="material-icons material-icons-outlined md-24">download_for_offline</i></span></a>⇥ Guide exploitation des aéronefs NCO<br>							
																		<a href="<?php echo $extStoragePath; ?>aca-pdf/ACA2403-DTO-Manuel DTO Ed3V0 du 6 mai 2022.pdf"
																		target="_blank">
																		<span><i
																				class="material-icons material-icons-outlined md-24">download_for_offline</i></span></a>⇥ Manuel du DTO								
										<br>
								<br>
								<br>
								<h5>Ici sont archivés les suivis de :</h5>
								<ul>
									<li>SGS, Notam</li>
									<li>Notes, internes mémos</li>
									<li>Veille règlementaire et documentaire</li>
									<li>Formation théorique LAPL et PPL</li>
									<li>Formation pratique LAPL</li>
									<li>Formation LAPL vers PPL</li>
									<li>Formation pratique PPL</li>
									<li>Qualification de type SEP (L) (renouvellement PPL)</li>
									<li>Qualification vol de nuit</li>
									<li>Vol de découverte</li>
									<li>Liste des CRESAG</li>
								</ul>
								</p>
							</div>
							<h5>Seuls les fichiers les plus récents (donc en vigueur) sont téléchargeables</h5>
							<div class="accordion">
								<ul class="accordion-list">
									<?php 
                                    	include("dto_accordion_01.php");
                                    	include("dto_accordion_02.php");
                                    	include("dto_accordion_03.php");
                                    	include("dto_accordion_04.php");
                                    	include("dto_accordion_05.php");
                                    	include("dto_accordion_06.php");
                               		?>
								</ul>
							</div>
						</div>
					</div>