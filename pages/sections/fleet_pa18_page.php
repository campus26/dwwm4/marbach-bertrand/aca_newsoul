<div class="section">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="news-post">
                                <header class="news-post-header">
                                    <h1 class="news-post-title">PA-18 type 150 N°18-3293 F-BFES</h1>
                                    <div class="news-post-meta">
                                        <div class="news-post-meta-item">
                                            <i class="material-icons md-22">access_time</i>
                                            <span>avant le 18 février 1967</span>
                                        </div>
                                        <div class="news-post-meta-item">
                                            <span>conception, construction : &nbsp;</span>
                                            <a href="#!">Piper Aircraft, Lock Haven, Pennsylvania</a>
                                        </div>
                                        <div class="news-post-meta-item">
                                            <i class="material-icons md-20">chat_bubble</i>
                                            <span>18 (mais on peut aussi écrire une petite accroche)</span>
                                        </div>
                                    </div>
                                    <div class="news-post-img item-bordered item-border-radius">
                                        <img data-src="../assets/img/ACA2109-FBFES.jpeg" class="img-responsive lazy"
                                            src="data:image/gif;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII="
                                            alt="">
                                    </div>
                                    <br>
                                </header>
                                <article class="news-post-article content">
                                   
                                    <h2>Documents et paramètres à considérer</h2>
        
                                    <blockquote>Ci-dessous les principales caractéristiques de l'aéronef et plus bas
                                        un tableau des principaux paramètres (liste non exhaustive) que le pilote 
                                        utilisera pour préparer et planifier son vol
                                    </blockquote>

                                    <ul>
                                        <li><a href="<?php echo $extStoragePath; ?>aca-pdf/ACA2403-FBFES-ManuelVol.pdf" download="F-ES Manuel de Vol.pdf" target="_blank">Manuel de Vol</a></li>
                                        <li><a href="<?php echo $extStoragePath; ?>aca-pdf/ACA0905-FBFES-Fiche-pesee.pdf" download="F-ES Fiche de pesée.pdf" target="_blank">Fiche de pesée</a></li>
                                        <li><a href="<?php echo $extStoragePath; ?>aca-pdf/ACA2403-FBFES-CL.pdf" download="F-ES Check List.pdf" target="_blank">Check-List</a></li>
                                        <li><a href="<?php echo $extStoragePath; ?>aca-pdf/ACA2403-FBFES-DossierVol.pdf" download="F-ES Dossier de vol.pdf" target="_blank">Dossier de Vol</a></li>
                                        <!-- <li><a href="<php echo $extStoragePath; ?>aca-pdf/ACA2403-FBFES-Phraseologie-de-base-v1.31.pdf" download="F-ES Phraséologie de base.pdf" target="_blank">Phraséologie de base</a></li> -->
                                        <li><a href="<?php echo $extStoragePath; ?>aca-pdf/ACA2403-FBFES-remplir-carnet-de-route.pdf" download="F-ES Memo carnet de route.pdf" target="_blank">Mémo carnet de route</a></li>
                                    </ul>
                                    <br>
                                    <table style="text-align:center">
                                        <thead>
                                            <tr>
                                                <th>Moteur</th>
                                                <th>Hélice</th>
                                                <th>Vx</th>
                                                <th>Vy</th>
                                                <th>Vi (75%)</th>
                                                <th>Carburant
                                                    total
                                                </th>
                                                <th>Carburant
                                                    inutil.</th>
                                                <th>Conso 75%
                                                    (2500t/min)
                                                </th>
                                                <th>Conso
                                                    (local)</th>
                                                <th>Conso
                                                    (TdP)
                                                </th>
                                                <th>Chge utile</th>
                                                <th>MTOW</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td >Lycoming<br>
                                                    O 320 A2B<br>
                                                    150CV
                                                </td>
                                                <td >Sensenich<br>
                                                    74 DM56
                                                </td>
                                                <td>113</td>
                                                <td>121</td>
                                                <td>170</td>
                                                <td>G 68<br>
                                                    D 68
                                                </td>
                                                <td>G 6<br>
                                                    D 0
                                                </td>
                                                <td>32</td>
                                                <td>30</td>
                                                <td>28</td>
                                                <td>299</td>
                                                <td>794</td>
                                            </tr>   
                                            <tr style="font-size: 0.8em; font-style: italic">
                                                <td style="font-size: 1em; text-align: right">unité</td>
                                                <td></td>
                                                <td >km/h</td>
                                                <td>km/h</td>
                                                <td>km/h</td>
                                                <td>L</td>
                                                <td>L</td>
                                                <td>L/h</td>
                                                <td>L/h</td>
                                                <td>L/h</td>
                                                <td>kg</td>
                                                <td>kg</td>
                                            </tr>                                        
                                        </tbody>
                                    </table>
                                    <p>Bonne préparation !</p>
                                    <p>L'équipe pédagogique et le mécano sont là pour vous aider à lever les doutes</p>
                                </article>
                                <footer class="news-post-footer">
                                    <div class="row align-items-center justify-content-between items">
                                        <div class="col-md col-12 item">
                                            <ul class="news-post-cat">
                                                <li><a href="../assets/img/ACA2109-FBFES-DB.jpg" target="_blank">Tableau de bord</a></li>
                                                <li><a href="<?php echo $extStoragePath; ?>aca-pdf/ACA2301-FBFES-VxVy-experimental.pdf" target="_blank">Détermination expérimentale Vx, Vy</a></li>
                                             </ul>
                                        </div>
                                    </div>
                                </footer>
                            </div>
                        </div>
                    </div>
                </div>
            </div>