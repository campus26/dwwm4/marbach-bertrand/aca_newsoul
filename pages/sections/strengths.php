<section class="section section-bgc">
				<div class="container">
					<div class="row litems">
						<div class="col-12">
							<div class="section-heading heading-center">
								<div class="section-subheading">nos atouts</div>
								<h2>Pour vous décider à venir nous voir</h2>
							</div>
						</div>
						<div class="col-lg-4 col-md-6 col-12 litem">
							<div class="ini">
								<div class="ini-count">01</div>
								<div class="ini-info">
									<h3 class="ini-heading item-heading-large">Flotte variée</h3>
									<div class="ini-desc">
										<p>5 avions en parfait état de vol</p>
									</div>
								</div>
							</div>
						</div>
						<div class="col-lg-4 col-md-6 col-12 litem">
							<div class="ini">
								<div class="ini-count">02</div>
								<div class="ini-info">
									<h3 class="ini-heading item-heading-large">Des adhérents engagés par leur passion du vol</h3>
									<div class="ini-desc">
										<p>Même hors-saison, il y aura toujours un pilote qualifié pour vous accompagner 
											lors d'un vol de découverte (Baptême de l'air).</p>
									</div>
								</div>
							</div>
						</div>
						<div class="col-lg-4 col-md-6 col-12 litem">
							<div class="ini">
								<div class="ini-count">03</div>
								<div class="ini-info">
									<h3 class="ini-heading item-heading-large">Une équipe pédagogique qualifiée</h3>
									<div class="ini-desc">
										<p>L'équipe des instructeurs accompagne aussi bien des ab-initio que des pilotes 
											déjà expérimentés durant tout le processus de vol</p>
									</div>
								</div>
							</div>
						</div>
						<div class="col-lg-4 col-md-6 col-12 litem">
							<div class="ini">
								<div class="ini-count">04</div>
								<div class="ini-info">
									<h3 class="ini-heading item-heading-large">Un environnement exceptionnel</h3>
									<div class="ini-desc">
										<p>En connaissez-vous beaucoup, des terrains nichés entre trois rivières à
											deux coudées d'un patrimoine mondial de l'Unesco ?</p>
									</div>
								</div>
							</div>
						</div>
						<div class="col-lg-4 col-md-6 col-12 litem">
							<div class="ini">
								<div class="ini-count">05</div>
								<div class="ini-info">
									<h3 class="ini-heading item-heading-large">Biodiversité</h3>
									<div class="ini-desc">
										<p>Il y a une rivière où vous pourrez vous relaxer juste à côté. </p>
										<p>Aussi, et peut-être d'abord, notre aérodrome est une magnifique réserve de  
											biodiversité : prairie, lande, forêt, rivières, gués et plus !
										</p>
									</div>
								</div>
							</div>
						</div>
						<div class="col-lg-4 col-md-6 col-12 litem">
							<div class="ini">
								<div class="ini-count">06</div>
								<div class="ini-info">
									<h3 class="ini-heading item-heading-large">Quelle vue, là-haut</h3>
									<div class="ini-desc">
										<p>Envolez-vous pour vérifier cela par vous-même. Et revenez en faire
											profiter vos amis et famille.</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>