<li class="accordion-item section-bgc">
									<div class="accordion-trigger">
										<div><a href="#!" target="_blank" title="" style="display: inline-block;">
												<img src="../assets/img/ACA2403-Icone-liste-de-choses-a-faire.png"
													alt="icone d'incident et d'alerte'" width="44" height="44">
											</a>∾ Notes internes, mémos
										</div>
									</div>
									<?php if ($grinchLevel <= 1400) { ?>
									<div class="accordion">
										<div class="row gutters-default">
											<div class="accordion-content content">
												<table class="table-secondary">
													<p>SGS : correspondant sécurité, RP : Responsable pédagogique</p>
													<thead>
														<tr>
															<th>Lien</th>
															<th>N°</th>
															<th>Date</th>
															<th>Origine</th>
															<th>Titre</th>
														</tr>
													</thead>
													<tbody>
														<tr>
															<td><a href="<?php echo $extStoragePath; ?>aca-pdf/ACA2403-DTO-SGS-Gestion-Carburant.pdf"
																target="_blank">
																<span><i
																		class="material-icons material-icons-outlined md-24">download_for_offline</i></span></a></td>
															<td>ACA-2024002-SGS</td>			
															<td>19/03/24</td>
															<td>SGS</td>
															<td>Suite CRESAG, Gestion Carburant</td>
														</tr>

														<tr>
															<td><a href="<?php echo $extStoragePath; ?>aca-pdf/ACA2403-DTO-RP-Disposition-ad-hoc-ecole.pdf"
																target="_blank">
																<span><i
																		class="material-icons material-icons-outlined md-24">download_for_offline</i></span></a></td>
																		<td>ACA-2023004-RP</td>
															<td>15/03/23</td>
															<td>SGS</td>
															<td>Disposition ad-hoc école</td>
														</tr>
														<tr>
															<td><a href="<?php echo $extStoragePath; ?>aca-pdf/ACA2403-DTO-RP-Lache-DR220-suite-refection.pdf"
																target="_blank">
																<span><i
																		class="material-icons material-icons-outlined md-24">download_for_offline</i></span></a></td>
																		<td>ACA-2023003-RP</td>
															<td>26/05/23</td>
															<td>SGS</td>
															<td>Re-lâché DR220 suite réfection</td>
														</tr>
														<tr>
															<td><a href="<?php echo $extStoragePath; ?>aca-pdf/ACA2403-DTO-RP-Lache-PA28-suite-nouvelle-avionique.pdf"
																target="_blank">
																<span><i
																		class="material-icons material-icons-outlined md-24">download_for_offline</i></span></a></td>
																		<td>ACA-2023002-RP</td>
															<td>23/03/23</td>
															<td>RP</td>
															<td>Re-lâché PA28 suite nouvelle avionique</td>
														</tr>
														<tr>
															<td><a href="<?php echo $extStoragePath; ?>aca-pdf/ACA2304-DTO-SGS-Invitation-au-voyage-bien-prepare.pdf"
																target="_blank">
																<span><i
																		class="material-icons material-icons-outlined md-24">download_for_offline</i></span></a></td>
																		<td>ACA-2023001-SGS</td>
															<td>15/03/23</td>
															<td>SGS</td>
															<td>Invitation au voyage... bien préparé</td>
														</tr>
														<tr>
															<td><a href="<?php echo $extStoragePath; ?>aca-pdf/ACA2403-DTO-RP-Consignes-pratiques-Automne-22v1.3.pdf"
																target="_blank">
																<span><i
																		class="material-icons material-icons-outlined md-24">download_for_offline</i></span></a></td>
																		<td>v1.2</td>
															<td>18/10/23</td>
															<td>RP</td>
															<td>Vigilance saisonnière - automne</td>
														</tr>
													</tbody>
												</table>
											</div>
										</div>
									</div>
									<?php } ?>
								</li>