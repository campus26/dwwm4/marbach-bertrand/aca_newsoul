<section class="section">
				<div class="container">
					<div class="row">
						<header class="col-12">
							<div class="section-heading heading-center">
								<div class="section-subheading">rejoignez-nous</div>
								<h2>pour un vol découverte d'1/2h...</h2>
								<h3>...ou pour plus longtemps !</h3>
							</div>
						</header>
						<div class="col-lg-4 col-md-6 col-12 item">
							<a href="../pages/ecole.php" class="iitem item-style iitem-hover">
								<div class="iitem-icon">
									<i class="material-icons material-icons-outlined md-48">school</i>
								</div>
								<div class="iitem-icon-bg">
									<i class="material-icons material-icons-outlined">cschool</i>
								</div>
								<h3 class="iitem-heading item-heading-large">Devenez pilote</h3>
								<div class="iitem-desc">de 7 à 77 ans, en vrai de 15 à passés 70 ans, il vous est
									possible d'apprendre à piloter et de décrocher votre licence (LAPL : Light Aircraft
									Pilote Licence, PPL : Private Pilote Licence). Bref devenez pilote privé dans notre DTO (école de pilotage). </div>
							</a>
						</div>
						<div class="col-lg-4 col-md-6 col-12 item">
							<a href="https://www.gorges-ardeche-pontdarc.fr/activite/les-gorges-de-lardeche-vues-du-ciel-labeaume/" target="_blank" class="iitem item-style iitem-hover">
								<div class="iitem-icon">
									<i class="material-icons material-icons-outlined md-48">vrpano</i>
								</div>
								<div class="iitem-icon-bg">
									<i class="material-icons material-icons-outlined">vrpano</i>
								</div>
								<h3 class="iitem-heading item-heading-large">Vol Découverte</h3>
								<div class="iitem-desc">Envolez-vous dès maintenant en réservant un vol découverte
									(Baptême de l'air).<br>30 minutes exceptionnelles de survol de la région, par exemple
									au dessus des Gorges de l'Ardèche et du pont naturel d'Arc.
								</div>
							</a>
						</div>
						<div class="col-lg-4 col-md-12 col-12 item">
							<a href="pages/contacts.html" class="iitem item-style iitem-hover">
								<div class="iitem-icon">
									<i class="material-icons material-icons-outlined md-48">flight_takeoff</i>
								</div>
								<div class="iitem-icon-bg">
									<i class="material-icons material-icons-outlined">flight_takeoff</i>
								</div>
								<h3 class="iitem-heading item-heading-large">Adhérez chez nous</h3>
								<div class="iitem-desc">Déjà pilote ? <br>Présentez-vous, contactez-nous et venez discuter
									avec nous. Vous serez certainement bienvenu(e) dans l'Aéroclub.</div>
							</a>
						</div>
						<div class="section-footer col-12 section-footer-animate">
							<div class="btn-group align-items-center justify-content-center">
								<a href="https://www.gorges-ardeche-pontdarc.fr/activite/les-gorges-de-lardeche-vues-du-ciel-labeaume/" target="_blank" class="btn btn-with-icon btn-w240 ripple">
									<span>Je réserve mon vol</span>
									<svg class="btn-icon-right" viewBox="0 0 13 9" width="13" height="9">
										<use xlink:href="../assets/img/sprite.svg#arrow-right"></use>
									</svg>
								</a>
							</div>
						</div>
					</div>
				</div>
			</section>