		<div class="bff">
			<div class="container">
				<div class="row">
					<div class="col">
						<div class="bff-container">
							<p>Get Full-Featured <br class="d-sm-none"> PathSoft Template</p>
							<div class="btn-group justify-content-center justify-content-md-start">
								<a href="#!"
									class="btn btn-border btn-with-icon btn-small ripple">
									<span>HTML5</span>
									<svg class="btn-icon-right" viewBox="0 0 13 9" width="13" height="9">
										<use xlink:href="../assets/img/sprite.svg#arrow-right"></use>
									</svg>
								</a>
								<a href="#!"
									class="btn btn-border btn-with-icon btn-small ripple">
									<span>WordPress</span>
									<svg class="btn-icon-right" viewBox="0 0 13 9" width="13" height="9">
										<use xlink:href="../assets/img/sprite.svg#arrow-right"></use>
									</svg>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>