                                            <li class="accordion-item section-bgc">
												<div class="accordion-trigger">
													<div><a href="#!" target="_blank" title=""
															style="display: inline-block;">
															<img src="../assets/img/ACA2403-Icone-6352994.png"
																alt="icone d'itinéraire de voyage" width="44"
																height="44">
														</a>∾ Remplir un plan de Vol (FPL)
													</div>
												</div>
												<div class="accordion-content content">
													<p>													
														<a href="../assets/media/pdf/ACA2403-FPL_(avec exemples LFHF).pdf"
															target="_blank">
															<span><i
																	class="material-icons material-icons-outlined md-24">download_for_offline</i></span></a>⇥ Description
														d'un plan de vol (nécessaire si survol de l'eau, franchissement de frontières, voyage de nuit)
													</p>
												</div>
											</li>