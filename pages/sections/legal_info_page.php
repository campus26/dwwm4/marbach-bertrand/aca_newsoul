            <div class="section">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="section-heading heading-center section-heading-animate">
                                <h1>Informations légales</h1>
                            </div>
                            <div class="content">
                                <p>Dernière mise à jour de cette page : 13 avril 2024</p>
                                <p>l'Aéroclub de l'Ardèche est une association à but non lucratif régie par les
                                    dispositions de la loi de 1901.</p>
                                <p>L'aéroclub de l'Ardèche est affilié à la Fédération Française d'Aéronautique</p></div>
                                <br>
                                
                                <h2>Mentions légales</h2>
                                <p>La loi nous demande de publier les informations suivantes. Nous en rajoutons une ou deux que nous pensons utiles.</p>
                                <h1>RGPD, CGV</h1>
                                <ul>
                                    <li>Contrairement aux fichiers clients des professionnels, le fichier d’une association 
                                        n’est pas soumis au régime de déclaration préalable à la CNIL.</li>
                                    <li>Une association n’a pas à publier de CGV (Conditions Générales de Vente), sauf en cas 
                                        de vente en ligne. Nous ne vendons pas en ligne, mais pointons vers un site, celui de l'office du tourisme</li>                                   
                                </ul>
                                
                                <h3>Association Aéro-Club de l'Ardèche</h3>
                                <ul>
                                    <li>
                                        <p><strong>Nom de l'association : </strong>Aéro-club de l'Ardèche</p>
                                    </li>
                                    <li>
                                        <p><strong>Adresse : </strong>560 chemin du flojas</p>
                                    </li>
                                    <li>
                                        <p><strong>Code Postal : </strong>07120 <strong>Localité : </strong>Labeaume</p>
                                    </li>
                                    <li>
                                        <p><strong>Numéro de téléphone - fixe : </strong>+33 4 75 39 62 57</p>
                                    </li>
                                    <li>
                                        <p><strong>Numéro de téléphone - mobile : </strong>+33 6 80 84 72 97</p>
                                    </li>
                                    <li>
                                        <p><strong>Directeur de la publication : </strong>Julien MEISTER</p>
                                    </li>
                                    </ul>

                                <h3>Hébergeur</h3>
                                    <ul>
                                    <li>
                                        <p><strong>Raison sociale : </strong>Nuxit</p>
                                    </li>
                                    <li>
                                        <p><strong>Adresse : </strong>97-97bis rue du Général Mangin</p>
                                    </li>
                                    <li>
                                        <p><strong>Code Postal : </strong>38100 <strong>Localité : </strong>Grenoble</p>
                                    </li>
                                    <li>
                                        <p><strong>Numéro de téléphone - fixe : </strong>+33 4 86576000</p>
                                    </li>
                                </ul>
                                
                                <h2>Contact</h2>
                                <p>Si vous avez des questions, nous sommes aussi joignables par courriel : 
                                    <a href="mailto:contact@aeroclubdelardeche.com">contact@aeroclubdelardeche.com</a></p>
                            </div>
                        </div>
                    </div>
                </div>
        </div>