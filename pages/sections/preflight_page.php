<section class="section">
				<div class="container">
					<div class="row">
						<div class="col-12">
							<div class="section-heading heading-center">
								<h2>Préparation des vols et des navigations</h2>
								<br>
								<div class="section-subheading">Si longtemps recherchées 🤭, les ressources de
									préparation des
									vols sont cachées ici !</div>
							</div>

							<p>Certains documents spécifiques à chaque avion sont à chercher dans
								les pages relatives à la flotte de l'aéroclub.</p>

							<div class="accordion">
								<div class="row gutters-default">
									<div class="col-lg-6 col-12">
										<ul class="accordion-list">
                                            <?php
											    // <!-- début 1G-->
											        include("preflight_prepa.php");
											    // <!-- fin 1G -->
                                                    
											    // <!-- début 2G -->
                                                    include("preflight_check_lists.php");
											    // <!-- fin 2G -->
                                             
											    // <!-- début 3G -->
                                                    include("preflight_home_receipes.php");
											    // <!-- fin 3G -->

											    // <!-- début 4G -->
                                                    include("preflight_discovery_flight.php");
											    // <!-- fin 4G -->
                                            ?>
										</ul>
									</div>
									<!-- fin colonne de gauche-->

									<!-- début colonne de droite -->
									<div class="col-lg-6 col-12">
										<ul class="accordion-list">
                                            <?php
											    // <!-- début 1D -->
                                                    include("preflight_flightlog.php");
											    // <!-- fin 1D -->

											    // <!-- début 2D -->
                                                    include("preflight_night_vfr.php");
											    // <!-- fin 2D -->

											    // <!-- début 3D -->
                                                    include("preflight_how_to_fpl.php");
											    // <!-- fin 3D -->

											    // <!-- début 4D -->
                                                    include("preflight_how_to_eccairs.php");
											    // <!-- fin 4D -->
                                            ?>
										</ul>
									</div>
									<!-- fin colonne de gauche-->
								</div>
							</div>
							<br>
							<br>
							<div class="item-bordered item-border-radius">
								<img src="../assets/img/ACA2403-FlotteCinqAvions.jpg" alt="" width="100%">
							</div>
						</div>
					</div>
				</div>
			</section>