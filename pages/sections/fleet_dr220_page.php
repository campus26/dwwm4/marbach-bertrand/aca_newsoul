<div class="section">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="news-post">
                                <header class="news-post-header">
                                    <h1 class="news-post-title">DR220/A N°91 F-BPCH</h1>
                                    <div class="news-post-meta">
                                        <div class="news-post-meta-item">
                                            <i class="material-icons md-22">access_time</i>
                                            <span>6 mars 1967</span>
                                        </div>
                                        <div class="news-post-meta-item">
                                            <span>conception, construction : &nbsp;</span>
                                            <a href="#!">DR : Delemontez et Robin, Centre Est Aéoronautique (Dijon)</a>
                                        </div>
                                        <div class="news-post-meta-item">
                                            <i class="material-icons md-20">chat_bubble</i>
                                            <span>18 (mais on peut aussi écrire une petite accroche)</span>
                                        </div>
                                    </div>
                                    <div class="news-post-img item-bordered item-border-radius">
                                        <img data-src="../assets/img/ACA2403-FBPCH-4921.jpeg" class="img-responsive lazy"
                                            src="data:image/gif;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII="
                                            alt="">
                                    </div>
                                    <br>
                                </header>
                                <article class="news-post-article content">
                                   
                                    <h2>Documents et paramètres à considérer</h2>
        
                                    <blockquote>Ci-dessous les principales caractéristiques de l'aéronef et plus bas
                                        un tableau des principaux paramètres (liste non exhaustive) que le pilote 
                                        utilisera pour préparer et planifier son vol
                                    </blockquote>

                                    <ul>
                                        <li><a href="<?php echo $extStoragePath; ?>aca-pdf/ACA2403-FBPCH-ManuelVol.pdf" download="F-CH Manuel de Vol.pdf" target="_blank">Manuel de Vol</a></li>
                                        <li><a href="<?php echo $extStoragePath; ?>aca-pdf/ACA2403-AV-30-Pilot_manual_anglais_Quick-Reference-Card.pdf" download="F-CH Manuel AV30.pdf" target="_blank">Supplément au  Manuel de Vol : AV30</a></li>
                                        <li><a href="<?php echo $extStoragePath; ?>aca-pdf/ACA1807-FBPCH-fiche-pesee.pdf"download="F-CH Fiche de pesée.pdf" target="_blank">Fiche de pesée</a></li>
                                        <li><a href="<?php echo $extStoragePath; ?>aca-pdf/ACA2403-FBPCH-CL.pdf" download="F-CH Check List.pdf" target="_blank">Check-List</a></li>
                                        <li><a href="<?php echo $extStoragePath; ?>aca-pdf/ACA2403-FBPCH-DossierVol.pdf" download="F-CH Dossier de vol.pdf" target="_blank">Dossier de Vol</a></li>
                                        <li><a href="<?php echo $extStoragePath; ?>aca-pdf/ACA2403-FBPCH-Phraseologie-de-base-v1.31.pdf" download="F-CH Phraséologie de base.pdf" target="_blank">Phraséologie de base</a></li>
                                        <li><a href="<?php echo $extStoragePath; ?>aca-pdf/ACA2403-FBPCH-remplir-carnet-de-route.pdf" download="F-CH Memo carnet de route.pdf" target="_blank">Mémo carnet de route</a></li>
                                    </ul>
                                    <br>
                                    <table style="text-align:center">
                                        <thead>
                                            <tr>
                                                <th>Moteur</th>
                                                <th>Hélice</th>
                                                <th>Vx</th>
                                                <th>Vy</th>
                                                <th>Vi (75%)</th>
                                                <th>Carburant
                                                    total
                                                </th>
                                                <th>Carburant
                                                    inutil.</th>
                                                <th>Conso 75%
                                                    (2500t/min)
                                                </th>
                                                <th>Conso
                                                    (local)</th>
                                                <th>Conso
                                                    (TdP)
                                                </th>
                                                <th>Chge utile</th>
                                                <th>MTOW</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td >Continental<br>
                                                    O 200 A<br>
                                                    101CV
                                                </td>
                                                <td >Evra bois<br>
                                                    D11 28 7C
                                                </td>
                                                <td>110</td>
                                                <td>130</td>
                                                <td>185</td>
                                                <td>110</td>
                                                <td>6</td>
                                                <td>22</td>
                                                <td>20</td>
                                                <td>18</td>
                                                <td>330</td>
                                                <td>780</td>
                                            </tr>   
                                            <tr style="font-size: 0.8em; font-style: italic">
                                                <td style="font-size: 1em; text-align: right">unité</td>
                                                <td></td>
                                                <td >km/h</td>
                                                <td>km/h</td>
                                                <td>km/h</td>
                                                <td>L</td>
                                                <td>L</td>
                                                <td>L/h</td>
                                                <td>L/h</td>
                                                <td>L/h</td>
                                                <td>kg</td>
                                                <td>kg</td>
                                            </tr>                                        
                                        </tbody>                                
                                    </table>
                                    <p>Bonne préparation !</p>
                                    <p>L'équipe pédagogique et le mécano sont là pour vous aider à lever les doutes</p>
                                </article>
                                <footer class="news-post-footer">
                                    <div class="row align-items-center justify-content-between items">
                                        <div class="col-md col-12 item">
                                            <ul class="news-post-cat">
                                                <li><a href="../assets/img/ACA2403-FBPCH-DB-projet-renovation.jpeg" target="_blank">Tableau de bord</a></li>
                                                <li><a href="<?php echo $extStoragePath; ?>aca-pdf/ACA2403-FBPCH-aera-660-User-Manual-190-02017-20-h.pdf" download="F-CH GPS aera 660.pdf" target="_blank">Manuel GPS aera 660 (eng)</a></li>
                                                <li><a href="../assets/img/ACA2403-FBPCH-avant-renovation.jpeg" target="_blank">F-CH avant rénovation</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </footer>
                            </div>
                        </div>
                    </div>
                </div>
            </div>