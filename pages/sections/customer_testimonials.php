<section class="section">
				<div class="container">
					<div class="row">
						<header class="col-12">
							<div class="section-heading heading-center">
								<div class="section-subheading">Quelques commentaires de nos passagers</div>
								<h2>morceaux choisis</h2>
							</div>
						</header>
						<div class="col-lg-4 col-md-6 col-12 item">
							<div class="reviews-item item-style">
								<div class="reviews-item-header">
									<div class="reviews-item-img">
										<img data-src="../assets/img/ACA2403auth-img-1.jpg" class="img-cover lazy" src="data:image/gif;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII=" alt="">
									</div>
									<div class="reviews-item-info">
										<h4 class="reviews-item-name item-heading">Pika Atchoum</h4>
										<div class="reviews-item-position">août 2023</div>
									</div>
								</div>
								<div class="reviews-item-text">
									<p>Accueil chaleureux. Tout est expliqué clairement pour apaiser les angoisses d'un premier vol en avion.<br>
										Et la découverte de l'Ardèche vu du ciel : magique<br>
										A un prix plus que raisonnable pour ce genre d'activité
									</p>
								</div>
							</div>
						</div>
						<div class="col-lg-4 col-md-6 col-12 item">
							<div class="reviews-item item-style">
								<div class="reviews-item-header">
									<div class="reviews-item-img">
										<img data-src="../assets/img/ACA2403auth-img-2.jpg" class="img-cover lazy" src="data:image/gif;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII=" alt="">
									</div>
									<div class="reviews-item-info">
										<h4 class="reviews-item-name item-heading">crmallef</h4>
										<div class="reviews-item-position">juillet 2023</div>
									</div>
								</div>
								<div class="reviews-item-text">
									<p>Ma femme, ma fille et moi-même avons effectué un vol de 30 minutes afin de découvrir la beauté des Gorges
										de l'Ardèche vues du ciel.<br>
										L'accueil fût excellent et le commandant de bord très sympathique.<br>
										Ce fût pour nous une première fois à un prix plutôt abordable (180 euros).<br>
										Expérience inoubliable et à renouveler.</p>
								</div>
							</div>
						</div>
						<div class="col-lg-4 col-md-12 col-12 item">
							<div class="reviews-item item-style">
								<div class="reviews-item-header">
									<div class="reviews-item-img">
										<img data-src="../assets/img/ACA2403auth-img-3.jpg" class="img-cover lazy" src="data:image/gif;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII=" alt="">
									</div>
									<div class="reviews-item-info">
										<h4 class="reviews-item-name item-heading">Mathilde Brochot</h4>
										<div class="reviews-item-position">avril 2023</div>
									</div>
								</div>
								<div class="reviews-item-text">
									<p>Inoubliable et à couper le souffle. Accueil très sympathique et chaleureux. On en prend plein la vue
										et c'est une activité qui vaut vraiment le détour.</p>
								</div>
							</div>
						</div>
						<footer class="section-footer col-12 section-footer-animate">
							<div class="btn-group align-items-center justify-content-center">
								<a href="https://www.gorges-ardeche-pontdarc.fr/activite/les-gorges-de-lardeche-vues-du-ciel-labeaume/" target="_blank" class="btn btn-with-icon btn-w240 ripple">
									<span>Je réserve</span>
									<svg class="btn-icon-right" viewBox="0 0 13 9" width="13" height="9">
										<use xlink:href="../assets/img/sprite.svg#arrow-right"></use>
									</svg>
								</a>
							</div>
						</footer>
					</div>
				</div>
			</section>