                                <!-- Formation Pratique PPL -->
                                <div class="tabs-item content">
                                    <p>Pour obtenir la licence de pilote privé d'avion PPL
                                        (<strong>P</strong>rivate <strong>P</strong>ilot
                                        <strong>L</strong>ilcence),
                                        afin d'être présenté à l'examen en vol, les conditions à réunir sont d'avoir :
                                    <h5>PPL (OACI)</h5>
                                    <ul>
                                        <li>une visite médicale aéronautique classe 2 en état de validité</li>
                                        <li>réussi l'examen théorique depuis moins de deux ans le jour du test en vol
                                        </li>
                                        <li>17 ans révolus le jour du test pratique</li>
                                        <li>navigué en solo vers deux aérodromes éloignés sur une distance totale d'au
                                            minimum 150 milles nautiques
                                        </li>
                                        <li>effectué des vols 'solo' pour un minimum de 10 heures totales (tours de
                                            piste, vol
                                            local, navigation au minimum de 5 heures)</li>
                                        <li>effectué au minimum 25h de vol en double commande</li>
                                        <li>effectué une formation d'une durée totale d'au minimim 45h (vols avec
                                            instructeur plus
                                            vols en solo)</li>
                                        </li>
                                    </ul>
                                    <br>

                                    <h5> Privilèges de la licence PPL </h5>
                                    <ul>
                                        Voler
                                        <li>à vue, de jour, en monomoteur (SEP) ou multimoteur (MEP) ou monoturbine
                                            (SET)</li>
                                        <li>dans des pays francophones adhérents à l'OACI</li>
                                        <li>dans des avions d'une masse maximale au décollage de 5,75 tonnes</li>
                                        <li>dans des avions transportant au maximum 19 passagers</li>
                                        <li>dans le cadre d'un vol non commercial (partage des frais directs de vol
                                            possible)
                                        </li>
                                    </ul>
                                    <br>

                                    <h5>Maintien d'exercice des privilèges de la PPL</h5>
                                    <ul>
                                        <li>avoir effectué dans les 12 derniers mois au minimum 12h de vol dont 6
                                            minimum comme commandant de bord
                                        </li>
                                        <li>avoir effectué un vol de contrôle avec instructeur dans un intervalle de
                                            maximum deux ans depuis le dernier contrôle</li>
                                        <li>ou avoir effectué un test en vol avec un examinateur si l'une des deux
                                            conditions ci-dessus n'était pas satisfaite</li>
                                        <li>visite médicale aéronautique classe 2 en état de validité</li>
                                        <li>avoir effectué, dans les trois derniers mois, trois atterrissages et trois
                                            décollages pour l'emport passager
                                        </li>
                                        <mark>Contrairement à la LAPL, le privilège d'emport passagers s'obtient dès la
                                            réussite à l'examen pratique.</mark>
                                    </ul>

                                    <br>
                                    <a href="<?php echo $extStoragePath; ?>aca-pdf/ACA2403-DTO-PPL-Cepadues-190100_poster des Matrices.pdf"
                                        target="_blank">
                                        <span><i
                                                class="material-icons material-icons-outlined md-24">download_for_offline</i></span></a>
                                    ⇥ À télécharger, le programme pratique suivi 'Cepadues'

                                    <div><?php if ($grinchLevel <= 1400) { ?><a href="<?php echo $extStoragePath; ?>aca-pdf/ACA2304-DTO-FORMATION_PRATIQUE_avec_liens-v2.23_230102.pdf"
                                            target="_blank" title="Télécharger le PDF" style="display: inline-block;">
                                            <img src="../assets/img/ACA2403-Icon-2116935.png"
                                                alt="feuille de route icônes" width="24" height="24">
                                        </a>
                                       
                                    ⇥ Résumé du parcours de progression pratique avec liens interactifs</div><?php } ?>
                                    <br><br>


                                    De plus, une PPL permettra plus facilement l'évolution vers la qualification du Vol
                                    de Nuit.
                                    <br>
                                    Enfin, <strong>contrairement à la LAPL qui ne le permet pas</strong>, la PPL offre l'évolution vers
                                    nombre de domaines
                                    supplémentaires : le pilotage multi-moteur, l'IFR, la turbine, la Montagne.<br>
                                    Elle est aussi un pré-requis en cas de souhait de carrière professionnelle.

                                    <br><br><br>
                                    <span><i
                                            class="material-icons material-icons-outlined md-24">warning_amber</i></span>Le
                                    nombre d'heures de vol nécessaires à un élève pilote pour être
                                    présenté à l'examen pratique varie en fonction d'un nombre non
                                    exhaustifs de paramètres tels que :

                                    <ul>
                                        <li>capacités techniques et non techniques</li>
                                        <li>âge</li>
                                        <li>disponibilité :
                                            <ul>
                                                <li>temporelle</li>
                                                <li>financière</li>
                                                <li>instructeur</li>
                                                <li>avion (mécanique, maintenance)</li>
                                            </ul>
                                        </li>
                                        <li>assiduité</li>
                                        <li>météo</li>
                                        <li>etc</li>
                                    </ul>
                                    Il est couramment admis qu'un projet tel que celui de passer sa licence de
                                    pilote prend du temps et que deux, trois ans sont très
                                    fréquemment observés.
                                    <br><br>
                                    La <strong>progression</strong> se fait selon un programme déposé auprès de la DGAC et <strong>la
                                    formation</strong> est un <mark>vrai travail d'équipe</mark> entre l'équipe pédagogique (le ou les
                                    instructeurs) et l'élève pilote.
                                    </p>
                                </div>