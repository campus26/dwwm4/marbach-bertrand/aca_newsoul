                                <!-- Perfectionnement PPL -->
                                <div class="tabs-item content">
                                    <p>Ailes ou phraséologie un peu ankilosées ?...<br>
                                        ou alors récemment breveté(e)...<br>
                                        ...avec le souhait de re-découvrir les joies de laisser le tour de piste un
                                        peu plus loin que d'habitude ? <br>
                                        <br>
                                        La navigation Ruoms-Aix les Milles-transit côtier via verticale Marignane-Nimes
                                        Garons-Ruoms sera le
                                        parfait support de la remise à niveau des compétences : <br>
                                        - navigation (ni trop, ni trop peu)<br>
                                        - VSV (pour se souvenir de l'importance des préaffichages)<br>
                                        - des points tournants de transits VFR (pour la précisions des cap et des
                                        estimées)<br>
                                        - négociation des transits en EAC (pour retrouver l'aisance à la radio)<br>
                                        <br>
                                        <?php if ($grinchLevel <= 1400) { ?>
                                        <a href="<?php echo $extStoragePath; ?>aca-pdf/Perfectionnement_PPLv1.0.pdf"
                                            target="_blank">
                                            <span><i
                                                    class="material-icons material-icons-outlined md-48">download_for_offline</i></span></a>⇥ Programme
                                        perfectionnement PPL<br>
                                        <?php } ?>

                                    <div class="image-container">
                                        <img src="../assets/img/ACA2403-DTO-J3.png"
                                            alt="Les trains classiques, c'est chouette aussi !">
                                    </div>
                                    </p>
                                </div>