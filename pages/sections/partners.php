<section class="section">
				<div class="container">
					<div class="row items">
						<div class="col-12">
							<div class="section-heading heading-center">
								<div class="section-subheading">Merci à nos soutiens</div>
								<h2>Quelques-uns de nos partenaires</h2>
								<p class="section-desc">Saviez-vous que le sud Ardèche était aussi dynamique que cela ? Regardez qui sont nos annonceurs
									et bien vite vous saurez pourquoi nous les recommandons.
									Revenez-sur cette page de temps en temps, nous vous en montrerons d'autres
								</p>
							</div>
						</div>
						<div class="col-lg-3 col-md-4 col-sm-4 col-6 item">
							<!-- Begin brands item -->
							<div class="brands-item item-style">
								<img data-src="../assets/img/brands/ACA2403annonceurMrBricolage.jpeg" class="lazy"
									src="data:image/gif;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII="
									alt="">
							</div>
							<a href="#!" class="btn btn-with-icon btn-w240 ripple"> 
								<span>Mr Bricolage Ruoms</span>
									<!-- <svg class="btn-icon-right" viewBox="0 0 13 9" width="13" height="9">
										<use xlink:href="../assets/img/sprite.svg#arrow-right"></use>
									</svg> -->
							</a>
							<!-- End brands item -->
						</div>
						<div class="col-lg-3 col-md-4 col-sm-4 col-6 item">
							<!-- Begin brands item -->
							<div class="brands-item item-style">
								<img data-src="../assets/img/brands/ACA2403annonceurReynouardTP.jpeg" class="lazy"
									src="data:image/gif;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII="
									alt="">
							</div>
							<a href="#!" class="btn btn-with-icon btn-w240 ripple">
								<span>TP Reynouard</span>
									<!-- <svg class="btn-icon-right" viewBox="0 0 13 9" width="13" height="9">
										<use xlink:href="../assets/img/sprite.svg#arrow-right"></use>
									</svg> -->
							</a>
							<!-- End brands item -->
						</div>
						<div class="col-lg-3 col-md-4 col-sm-4 col-6 item">
							<!-- Begin brands item -->
							<div class="brands-item item-style">
								<img data-src="../assets/img/brands/ACA2403annonceurCampingLaGrandTerre.jpeg" class="lazy"
									src="data:image/gif;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII="
									alt="">
							</div>
							<a href="https://www.campinglagrandterre.com/" target="_blank" class="btn btn-with-icon btn-w240 ripple">
								<span>Camping Grand Terre</span>
									<svg class="btn-icon-right" viewBox="0 0 13 9" width="13" height="9">
										<use xlink:href="../assets/img/sprite.svg#arrow-right"></use>
									</svg>
							</a>
							<!-- End brands item -->
						</div>
						<div class="col-lg-3 col-md-4 col-sm-4 col-6 item">
							<!-- Begin brands item -->
							<div class="brands-item item-style">
								<img data-src="../assets/img/brands/ACA2403annonceurHotelBarGlacierLaGarenne.jpeg" class="lazy"
									src="data:image/gif;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII="
									alt="">
							</div>
							<a href="https://www.hotel-ardechesud.com/" target="_blank" class="btn btn-with-icon btn-w240 ripple">
								<span>Hotel Glacier La Garenne</span>
									<svg class="btn-icon-right" viewBox="0 0 13 9" width="13" height="9">
										<use xlink:href="../assets/img/sprite.svg#arrow-right"></use>
									</svg>
							</a>
							<!-- End brands item -->
						</div>
						<div class="col-lg-3 col-md-4 col-sm-4 col-6 item">
							<!-- Begin brands item -->
							<div class="brands-item item-style">
								<img data-src="../assets/img/brands/ACA2403annonceurArdecheBoissonsDistributionMontanerPietrini.jpeg" class="lazy"
									src="data:image/gif;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII="
									alt="">
							</div>
							<a href="#!" class="btn btn-with-icon btn-w240 ripple"> 
								<span>Ardèche Boissons</span>
									<!-- <svg class="btn-icon-right" viewBox="0 0 13 9" width="13" height="9">
										<use xlink:href="../assets/img/sprite.svg#arrow-right"></use>
									</svg> -->
							</a>
							<!-- End brands item -->
						</div>
						<div class="col-lg-3 col-md-4 col-sm-4 col-6 item">
							<!-- Begin brands item -->
							<div class="brands-item item-style">
								<img data-src="../assets/img/brands/ACA2403annonceurOrpiLagerge.jpeg" class="lazy"
									src="data:image/gif;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII="
									alt="">
							</div>
							<a href="https://www.orpi.com/vielfaure" target="_blank" class="btn btn-with-icon btn-w240 ripple">
								<span>Orpi Ruoms</span>
									<svg class="btn-icon-right" viewBox="0 0 13 9" width="13" height="9">
										<use xlink:href="../assets/img/sprite.svg#arrow-right"></use>
									</svg>
							</a>
							<!-- End brands item -->
						</div>
						<div class="col-lg-3 col-md-4 col-sm-4 col-6 item">
							<!-- Begin brands item -->
							<div class="brands-item item-style">
								<img data-src="../assets/img/brands/ACA2403annonceurPiscinesSerre.jpeg" class="lazy"
									src="data:image/gif;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII="
									alt="">
							</div>
							<a href="https://www.drive-piscine-ruoms.com" target="_blank" class="btn btn-with-icon btn-w240 ripple">
								<span>Piscine Spas Serre</span>
									<svg class="btn-icon-right" viewBox="0 0 13 9" width="13" height="9">
										<use xlink:href="../assets/img/sprite.svg#arrow-right"></use>
									</svg>
							</a>
							<!-- End brands item -->
						</div>
						<div class="col-lg-3 col-md-4 col-sm-4 col-6 item">
							<!-- Begin brands item -->
							<div class="brands-item item-style">
								<img data-src="../assets/img/brands/ACA2403annonceurDomainedeChaussy.jpeg" class="lazy"
									src="data:image/gif;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII="
									alt="">
							</div>
							<a href="https://www.domainedechaussy.com/" target="_blank" class="btn btn-with-icon btn-w240 ripple">
								<span>Domaine de Chaussy</span>
									<svg class="btn-icon-right" viewBox="0 0 13 9" width="13" height="9">
										<use xlink:href="../assets/img/sprite.svg#arrow-right"></use>
									</svg>
							</a>
							<!-- End brands item -->
						</div>
					</div>
				</div>
			</section>