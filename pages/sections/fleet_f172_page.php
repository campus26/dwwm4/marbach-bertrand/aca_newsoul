<div class="section">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="news-post">
                                <header class="news-post-header">
                                    <h1 class="news-post-title">F172L N°990 F-BTUL</h1>
                                    <div class="news-post-meta">
                                        <div class="news-post-meta-item">
                                            <i class="material-icons md-22">access_time</i>
                                            <span>15 novembre 1971</span>
                                        </div>
                                        <div class="news-post-meta-item">
                                            <span>conception, construction : &nbsp;</span>
                                            <a href="#!">Cessna, Reims aviation (Prunay)</a>
                                        </div>
                                        <div class="news-post-meta-item">
                                            <i class="material-icons md-20">chat_bubble</i>
                                            <span>18 (mais on peut aussi écrire une petite accroche)</span>
                                        </div>
                                    </div>
                                    <div class="news-post-img item-bordered item-border-radius">
                                        <img data-src="../assets/img/ACA2403-FBTUL.jpeg" class="img-responsive lazy"
                                            src="data:image/gif;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII="
                                            alt="">
                                    </div>
                                    <br>
                                </header>
                                <article class="news-post-article content">
                                   
                                    <h2>Documents et paramètres à considérer</h2>
        
                                    <blockquote>Ci-dessous les principales caractéristiques de l'aéronef et plus bas
                                        un tableau des principaux paramètres (liste non exhaustive) que le pilote 
                                        utilisera pour préparer et planifier son vol
                                    </blockquote>

                                    <ul>
                                        <li><a href="<?php echo $extStoragePath; ?>aca-pdf/ACA2403-FBTUL-ManuelVol.pdf" download="F-UL Manuel de Vol.pdf" target="_blank">Manuel de Vol</a></li>
                                        <li><a href="<?php echo $extStoragePath; ?>aca-pdf/ACA2403-G5-UserManual(eng)-190-01112-12_A.pdf" download="F-UL Manuel G5.pdf" target="_blank">Supplément au  Manuel de Vol : G5</a></li>
                                        <li><a href="<?php echo $extStoragePath; ?>aca-pdf/ACA2403-AV-30-Pilot_manual_anglais_Quick-Reference-Card.pdf" download="F-UL Manuel AV30.pdf" target="_blank">Supplément au  Manuel de Vol : AV30</a></li>
                                        <li><a href="<?php echo $extStoragePath; ?>aca-pdf/ACA1807-FBTUL-fiche-pesee.pdf" download="F-UL Fiche de pesée.pdf" target="_blank">Fiche de pesée</a></li>
                                        <li><a href="<?php echo $extStoragePath; ?>aca-pdf/ACA2403-FBTUL-CL.pdf" download="F-UL Check List.pdf" target="_blank">Check-List</a></li>
                                        <li><a href="<?php echo $extStoragePath; ?>aca-pdf/ACA2403-FBTUL-DossierVol.pdf" download="F-UL Dossier de vol.pdf" target="_blank">Dossier de Vol</a></li>
                                        <!-- <li><a href="<php echo $extStoragePath; ?>aca-pdf/ACA2403-FBTUL-Phraseologie-de-base-v1.31.pdf" download="F-CH Phraséologie de base.pdf" target="_blank">Phraséologie de base</a></li> -->
                                        <li><a href="<?php echo $extStoragePath; ?>aca-pdf/ACA2403-FBTUL-remplir-carnet-de-route.pdf" download="F-UL Memo carnet de route.pdf" target="_blank">Mémo carnet de route</a></li>
                                    </ul>
                                    <br>
                                    <table style="text-align:center">
                                        <thead>
                                            <tr>
                                                <th>Moteur</th>
                                                <th>Hélice</th>
                                                <th>Vx</th>
                                                <th>Vy</th>
                                                <th>Vi (75%)</th>
                                                <th>Carburant
                                                    total
                                                </th>
                                                <th>Carburant
                                                    inutil.</th>
                                                <th>Conso 75%
                                                    (2500t/min)
                                                </th>
                                                <th>Conso
                                                    (local)</th>
                                                <th>Conso
                                                    (TdP)
                                                </th>
                                                <th>Chge utile</th>
                                                <th>MTOW</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td >Lycoming<br>
                                                    O 320 E2D<br>
                                                    150CV
                                                </td>
                                                <td >Mac Cauley<br>
                                                    1 C 160<br>
                                                    CTM 7553
                                                </td>
                                                <td>59</td>
                                                <td>75</td>
                                                <td>105</td>
                                                <td>G 98 <br>
                                                    D 98 </td>
                                                <td>G 7,5<br>
                                                    D 7,5</td>
                                                <td>30</td>
                                                <td>28</td>
                                                <td>26</td>
                                                <td>392</td>
                                                <td>1043</td>
                                            </tr>   
                                            <tr style="font-size: 0.8em; font-style: italic">
                                                <td style="font-size: 1em; text-align: right">unité</td>
                                                <td></td>
                                                <td>kn</td>
                                                <td>kn</td>
                                                <td>kn</td>
                                                <td>L</td>
                                                <td>L</td>
                                                <td>L/h</td>
                                                <td>L/h</td>
                                                <td>L/h</td>
                                                <td>kg</td>
                                                <td>kg</td>
                                            </tr>                                        
                                        </tbody>
                                    </table>
                                    <p>Bonne préparation !</p>
                                    <p>L'équipe pédagogique et le mécano sont là pour vous aider à lever les doutes</p>
                                </article>
                                <footer class="news-post-footer">
                                    <div class="row align-items-center justify-content-between items">
                                        <div class="col-md col-12 item">
                                            <ul class="news-post-cat">
                                                <li><a href="../assets/img/ACA2403-FBTUL-DB-blanc.jpg" target="_blank">Tableau de bord</a></li>
                                                <li><a href="https://storage.marbach-bertrand.ovh/aca-video/ACA2403-GNS430-Mise-en-route-initiale.mp4" target="_blank">GNS430•mise en route initiale</a></li>
                                                <li><a href="https://storage.marbach-bertrand.ovh/aca-video/ACA2403-GNS430-Consultation-ajustement-FuelOnBoard_FOB.mp4" target="_blank">GNS430•Ajuster F.O.B.</a></li>
                                                <li><a href="https://storage.marbach-bertrand.ovh/aca-video/ACA2403-GNS430-Changement-unite_Kt-MPH-Kmh.mp4" target="_blank">GNS430•Changer les unités</a></li>
                                                <li><a href="https://storage.marbach-bertrand.ovh/aca-video/ACA2403-G5-Changement-unite_Kt-MPH_Kmh.mp4" target="_blank">G5•Changer les unités</a></li>
                                                <li><a href="https://storage.marbach-bertrand.ovh/aca-video/ACA2403-AV30-GNS430-GPS-HSIetDirect-To.mp4" target="_blank">AV30•DirectTO avec GNS430</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </footer>
                            </div>
                        </div>
                    </div>
                </div>
            </div>