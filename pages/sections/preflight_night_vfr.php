                                            <li class="accordion-item section-bgc">
												<div class="accordion-trigger">
													<div><a href="#!" target="_blank" title=""
															style="display: inline-block;">
															<img src="../assets/img/ACA2403-Icone-2332243.png"
																alt="icone d'itinéraire de voyage" width="44"
																height="44">
														</a>∾ Vol de Nuit
													</div>
												</div>
												<div class="accordion-content content">
													<p>
														<a href="../assets/media/pdf/ACA2403-DTO-VDN_ENR_3_3_LFMM_SOUTH_ITI_NIGHT_VFR.pdf"
															target="_blank">
															<span><i
																	class="material-icons material-icons-outlined md-24">download_for_offline</i></span></a>⇥ Itinéraires
														VFR Nuit Sud
														<br>
														<a 
															<?php if ($grinchLevel <= 1400) { ?>
															href="<?php echo $extStoragePath; ?>aca-pdf/ACA2403-VDN_MasterTable.pdf"
															target="_blank">
															<?php } ?>
															<span><i
																	class="material-icons material-icons-outlined md-24">download_for_offline</i></span></a>⇥ Tableau
														des préaffichages (Fly By Numbers)
													</p>
												</div>
											</li>