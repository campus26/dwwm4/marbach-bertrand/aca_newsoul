<div class="section">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="news-post">
                                <header class="news-post-header">
                                    <h1 class="news-post-title">PA28-181 N°28-8890131 F-GKEF</h1>
                                    <div class="news-post-meta">
                                        <div class="news-post-meta-item">
                                            <i class="material-icons md-22">access_time</i>
                                            <span>27 février 1980</span>
                                        </div>
                                        <div class="news-post-meta-item">
                                            <span>conception, construction : &nbsp;</span>
                                            <a href="#!">Piper Aircraft, Vero Beach, FL (USA)</a>
                                        </div>
                                        <div class="news-post-meta-item">
                                            <i class="material-icons md-20">chat_bubble</i>
                                            <span>18 (mais on peut aussi écrire une petite accroche)</span>
                                        </div>
                                    </div>
                                    <div class="news-post-img item-bordered item-border-radius">
                                        <img data-src="../assets/img/ACA2403-FGKEF.jpeg" class="img-responsive lazy"
                                            src="data:image/gif;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII="
                                            alt="">
                                    </div>
                                    <br>
                                </header>
                                <article class="news-post-article content">
                                   
                                    <h2>Documents et paramètres à considérer</h2>
        
                                    <blockquote>Ci-dessous les principales caractéristiques de l'aéronef et plus bas
                                        un tableau des principaux paramètres (liste non exhaustive) que le pilote 
                                        utilisera pour préparer et planifier son vol
                                    </blockquote>

                                    <ul>
                                        <li><a href="<?php echo $extStoragePath; ?>aca-pdf/ACA2403-FGKEF-ManuelVol.pdf" download="F-EF Manuel de Vol.pdf" target="_blank">Manuel de Vol</a></li>
                                        <li><a href="<?php echo $extStoragePath; ?>aca-pdf/ACA2403-G5-UserManual(eng)-190-01112-12_A.pdf" download="F-EF Manuel G5.pdf" target="_blank">Supplément au  Manuel de Vol : G5</a></li>
                                        <li><a href="<?php echo $extStoragePath; ?>aca-pdf/ACA2403-FGKEF-Manuel-GTN650-Fr.pdf" download="F-EF Manuel GTN650xi.pdf" target="_blank">Supplément au  Manuel de Vol : GTN650xi</a></li>
                                        <li><a href="<?php echo $extStoragePath; ?>aca-pdf/ACA2403-FGKEF-Garmin-GFC-500-UserNotice.pdf" download="F-EF Manuel GFC 500.pdf" target="_blank">Supplément au  Manuel de Vol : GFC500</a></li>
                                        <li><a href="<?php echo $extStoragePath; ?>aca-pdf/ACA0908-FGKEF-fiche-pesee.pdf" download="F-EF Fiche de pesée.pdf" target="_blank">Fiche de pesée</a></li>
                                        <li><a href="<?php echo $extStoragePath; ?>aca-pdf/ACA2403-FGKEF-CL.pdf" download="F-EF Check List.pdf" target="_blank">Check-List</a></li>
                                        <li><a href="<?php echo $extStoragePath; ?>aca-pdf/ACA2403-FGKEF-DossierVol.pdf" download="F-EF Dossier de vol.pdf" target="_blank">Dossier de Vol</a></li>
                                        <!-- <li><a href="php echo $extStoragePath; ?>aca-pdf/ACA2403-FGKEF-Phraseologie-de-base-v1.31.pdf" download="F-EF Phraséologie de base.pdf" target="_blank">Phraséologie de base</a></li> -->
                                        <li><a href="<?php echo $extStoragePath; ?>aca-pdf/ACA2403-FGKEF-remplir-carnet-de-route.pdf" download="F-EF Memo carnet de route.pdf" target="_blank">Mémo carnet de route</a></li>
                                    </ul>
                                    <br>
                                    <table style="text-align:center">
                                        <thead>
                                            <tr>
                                                <th>Moteur</th>
                                                <th>Hélice</th>
                                                <th>Vx</th>
                                                <th>Vy</th>
                                                <th>Vi (75%)</th>
                                                <th>Carburant
                                                    total
                                                </th>
                                                <th>Carburant
                                                    inutil.</th>
                                                <th>Conso 75%
                                                    (2500t/min)
                                                </th>
                                                <th>Conso
                                                    (local)</th>
                                                <th>Conso
                                                    (TdP)
                                                </th>
                                                <th>Chge utile</th>
                                                <th>MTOW</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td >Lycoming<br>
                                                    O 360 A4A<br>
                                                    180CV
                                                </td>
                                                <td >Sensenich<br>
                                                    76EM855 0 62
                                                </td>
                                                <td>64</td>
                                                <td>75</td>
                                                <td>115</td>
                                                <td>G 94<br>
                                                    D 94 </td>
                                                <td>G 4<br>
                                                    D 4
                                                </td>
                                                <td>38</td>
                                                <td>35</td>
                                                <td>30</td>
                                                <td>424</td>
                                                <td>1156</td>
                                            </tr>   
                                            <tr style="font-size: 0.8em; font-style: italic">
                                                <td style="font-size: 1em; text-align: right">unité</td>
                                                <td></td>
                                                <td>kn</td>
                                                <td>kn</td>
                                                <td>kn</td>
                                                <td>L</td>
                                                <td>L</td>
                                                <td>L/h</td>
                                                <td>L/h</td>
                                                <td>L/h</td>
                                                <td>kg</td>
                                                <td>kg</td>
                                            </tr>                                        
                                        </tbody>
                                        </tfoot>
                                    </table>
                                    <p>Bonne préparation !</p>
                                    <p>L'équipe pédagogique et le mécano sont là pour vous aider à lever les doutes</p>
                                </article>
                                <footer class="news-post-footer">
                                    <div class="row align-items-center justify-content-between items">
                                        <div class="col-md col-12 item">
                                            <ul class="news-post-cat">
                                                <li><a href="../assets/img/ACA2403-FGKEF-DB-blanc.jpg" target="_blank">Tableau de bord</a></li>
                                                <li><a href="https://storage.marbach-bertrand.ovh/aca-video/ACA2403-G5-Changement-unite_Kt-MPH_Kmh.mp4" target="_blank">changer les unités du G5</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </footer>
                            </div>
                        </div>
                    </div>
                </div>
            </div>