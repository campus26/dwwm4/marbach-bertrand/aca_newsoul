                                <!-- BIA -->
                                <div class="tabs-item content active">
                                    <style>
                                        .image-container {
                                            text-align: center;
                                            margin-top: 20px;
                                        }

                                        .image-container img {
                                            width: 40%;
                                            display: block;
                                            margin: 0 auto;
                                        }
                                    </style>
                                    <h5>Présentation du BIA</h5>
                                    <p>La France est porteuse d’une véritable culture scientifique et technique de
                                        l’aéronautique et du spatial où se mêlent toutes à la fois une aviation sportive
                                        et de loisir, variée et vivante, une aviation militaire prestigieuse, une
                                        aviation
                                        civile dynamique et innovante.
                                        <br>
                                        La diversité des métiers, des pratiques, professionnelles ou amateurs, est
                                        animée par une même passion et une grande exigence de rigueur.<br>
                                        Le secteur aéronautique est un des secteurs les plus dynamiques de l'industrie
                                        et du commerce français. Nos avionneurs sont à la pointe de la technologie et
                                        beaucoup d'innovations dans ce domaine ont été et sont françaises. Il ne pourra
                                        qu'être fructueux de proposer au plus grand nombre une initiation à la
                                        culture scientifique et technique aéronautique et spatiale à la croisée des
                                        secteurs professionnels, sportifs et éducatifs.
                                    </p>

                                    <div class="image-container">
                                        <img src="../assets/img/ACA2403-BIA-image62.png"
                                            alt="les 4 forces de l'aérodynamisme">
                                    </div>

                                    <h5>À quoi sert le BIA ?</h5>

                                    Le BIA remplit l’obligation d’une formation théorique pour obtenir
                                    l’Autorisation de Base (LAPL) qui permet à un élève pilote en formation
                                    de voler seul aux commandes d’un avion dans un rayon de 25 milles nautiques
                                    autour de son aérodrome.


                                    <h5>Comment préparer le BIA ?</h5>

                                    <p>Il faut suivre une quarantaine d’heures de cours théoriques centrés sur la
                                        culture scientifique et technique dans les domaines de l’aéronautique et du
                                        spatial, et une information sur les différents métiers et formations.</p>

                                    <h5>Il y a 5 domaines d’étude :</h5>
                                    <ul>
                                        <li>Aérodynamique, aérostatique et principes du vol</li>
                                        <li>Météorologie et aérologie</li>
                                        <li>Navigation, réglementation et sécurité des vols</li>
                                        <li>Conaissance des aéronefs et des engins spatiaux</li>
                                        <li>Histoire et culture de l’aéronautique et du spatial</li>
                                    </ul>
                                    <br>
                                    <p>
                                        Les cours de BIA sont assurés chaque mardi hors congés scolaires à
                                        l'aéroclub.<br>
                                        Un complément en ligne est accessible
                                        <a href="https://pedagogie.ac-montpellier.fr/documents-pour-animer-une-formation-bia"
                                            target="_blank"> <span>sur le site de l'université de Montpellier</span></a>
                                    <div class="image-container">
                                        <img src="../assets/img/ACA2403-BIA-image42.jpeg"
                                            alt="les 4 forces de l'aérodynamisme">
                                    </div>
                                    </p>
                                    <h5>Bibliothèque</h5>
                                    <h6>Manuel du BIA</h6>
                                    <p>Le manuel du BIA est ici : <br><a
                                            href="<?php echo $extStoragePath; ?>aca-pdf/ACA2403-BIA-manuel-03-10-_1020001.pdf"
                                            target="_blank">
                                            <span><i
                                                    class="material-icons material-icons-outlined md-48">download_for_offline</i></span></a>
                                    </p>
                                    <h6>Présentations à télécharger</h6>
                                    <p><a href="<?php echo $extStoragePath; ?>aca-ppt/ACA2403-BIA-manuel_BIA_CIRAS_Toulouse_aerodynamique_mecavol_spatial_1025350.pptx"
                                            target="_blank">
                                            <span><i
                                                    class="material-icons material-icons-outlined md-24">download_for_offline</i></span></a>
                                        Aérodynamique<br><a
                                            href="<?php echo $extStoragePath; ?>aca-ppt/ACA2403-BIA-manuel_BIA_CIRAS_Toulouse_meteorologie_1025356.pptx"
                                            target="_blank">
                                            <span><i
                                                    class="material-icons material-icons-outlined md-24">download_for_offline</i></span></a>
                                        Météorologie<br><a
                                            href="<?php echo $extStoragePath; ?>aca-ppt/ACA2403-BIA-manuel_BIA_CIRAS_Toulouse_navigation_reglementation_1025358.pptx"
                                            target="_blank">
                                            <span><i
                                                    class="material-icons material-icons-outlined md-24">download_for_offline</i></span></a>
                                        Navigation<br><a
                                            href="<?php echo $extStoragePath; ?>aca-ppt/ACA2403-BIA-manuel_BIA_CIRAS_Toulouse_connaissance_aeronefs_1025352.pptx"
                                            target="_blank">
                                            <span><i
                                                    class="material-icons material-icons-outlined md-24">download_for_offline</i></span></a>
                                        Connaissance des aéronefs<br><a
                                            href="<?php echo $extStoragePath; ?>aca-ppt/ACA2403-BIA-manuel_BIA_CIRAS_Toulouse_histoire_1025354.pptx"
                                            target="_blank">
                                            <span><i
                                                    class="material-icons material-icons-outlined md-24">download_for_offline</i></span></a>
                                        Histoire de l'aéronautique</p>

                                </div>