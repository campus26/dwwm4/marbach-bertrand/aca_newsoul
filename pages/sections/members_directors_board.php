            <div class="section">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                                <h2>Membres du Bureau</h2>
                                <ul>
                                    <li>Président, MEISTER Julien</li>
                                    <li>Vice-Président, ABEILLON Paul</li>
                                    <li>Trésorier, BERTRAND Pierre</li>
                                    <li>Secrétaire, correspondant sécurité, LAFFONT Vincent</li>
                                    <li>Mécanicien, THIBON Marc (invité permanent)</li>
                                    <li>Responsable pédagogique, MARBACH Bertrand (invité permanent)</li>
                                    <li>secrétaire co-optée, THIBON Christine (invitée permanente)</li>
                                </ul>
                        </div>
                    </div>
                </div>
            </div>

