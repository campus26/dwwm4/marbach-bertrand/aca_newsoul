<li class="accordion-item section-bgc">
									<div class="accordion-trigger">
										<div><a href="#!" target="_blank" title="" style="display: inline-block;">
												<img src="../assets/img/ACA2403-Icone-livre.png"
													alt="icone d'incident et d'alerte'" width="44" height="44">
											</a>∾ SGS, Notam
										</div>
									</div>
									<?php if ($grinchLevel <= 1400) { ?>
									<div class="accordion">
										<div class="row gutters-default">
											<div class="accordion-content content">
												<p><a href="<?php echo $extStoragePath; ?>aca-pdf/ACA2403-DTO-SGS-REV-0-Version-1-05-Septembre-2018.pdf"
														target="_blank">
														<span><i
																class="material-icons material-icons-outlined md-24">download_for_offline</i></span></a>⇥ 180925-Fichier SGS
																<br>
																<a href="<?php echo $extStoragePath; ?>aca-pdf/ACA2403-DTO-SGS-guide_culture_juste.pdf"
																target="_blank">
																<span><i
																		class="material-icons material-icons-outlined md-24">download_for_offline</i></span></a>⇥ Guide de la culture juste				
												</p>
											</div>
										</div>
									</div>	
									<?php } ?>								
								</li>