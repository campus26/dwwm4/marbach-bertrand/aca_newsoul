                                            <li class="accordion-item section-bgc">
												<div class="accordion-trigger">
													<div><a href="#!" target="_blank" title=""
															style="display: inline-block;">
															<img src="../assets/img/ACA3403-Icone-destination.png"
																alt="icone d'itinéraire de voyage" width="44"
																height="44">
														</a>∾ Découvrez nos vols de découverte (<em>'baptême de l'air'</em>)
													</div>
												</div>
												<div class="accordion-content content">
													<p><?php if ($grinchLevel <= 1400) { ?>
														<a
															href="<?php echo $extStoragePath; ?>aca-pdf/ACA2403-DTO-Vols-de-decouverte-v2_0_20230606.pdf"
															target="_blank">
															<span><i
																	class="material-icons material-icons-outlined md-24">download_for_offline</i></span></a>⇥ Vol
														de découverte (2023)
														<br>
														<?php } ?>
														<a href="../assets/img/ACA2403-DTO-Tarif-VD-2024.png"
															target="_blank">
															<span><i
																	class="material-icons material-icons-outlined md-24">download_for_offline</i></span></a>⇥ Tarifs
														2024
													</p>
												</div>
											</li>