                                <!-- "Lâché" machine" -->
                                <div class="tabs-item content">
                                    <p>Nouvel(e) adhérent(e)...<br>
                                        pareil pour un(e) adhérent(e) dejà présent(e)...<br>
                                        ou alors récemment breveté(e)...<br>
                                        ...avec le souhait de découvrir une nouvelle machine ? <br>
                                        <br>
                                        Alors vous aurez à coeur de vous
                                        faire lâcher en ayant bien pris
                                        au préalable ce programme pour que ce nouvel avion ne fasse que vous ravir dès
                                        votre lâché machine !<br>
                                        <br>
                                        <?php if ($grinchLevel <= 1400) { ?>
                                        <a href="<?php echo $extStoragePath; ?>aca-pdf/ACA2403-DTO-Lache-machine_v1.2_230411.pdf"
                                            target="_blank">
                                            <span><i
                                                    class="material-icons material-icons-outlined md-48">download_for_offline</i></span></a>⇥ Programme
                                        lâché machine<br>
                                        <?php } ?>

                                    <div class="image-container">
                                        <img src=".././assets/img/ACA2403-DTO-J3.png"
                                            alt="Les trains classiques, c'est chouette aussi !">
                                    </div>
                                    </p>
                                </div>