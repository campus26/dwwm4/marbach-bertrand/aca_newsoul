<li class="accordion-item section-bgc">
									<div class="accordion-trigger">
										<div><a href="#!" target="_blank" title="" style="display: inline-block;">
												<img src="../assets/img/ACA2403-Icone-liste-de-controle.png"
													alt="icone d'incident et d'alerte'" width="44" height="44">
											</a>∾ Veille règlementaire et documentaire
										</div>
									</div>
									<?php if ($grinchLevel <= 1031) { ?>
									<div class="accordion">
										<div class="row gutters-default">
											<div class="accordion-content content">
												<table class="table-secondary">
													<thead>
														<tr>
															<th>Lien</th>
															<th>Date</th>
															<th>DGAC</th>
															<th>Météor</th>
															<th>ANPI</th>
															<th>FFA</th>
															<th>Aérogligli</th>
															<th>divers</th>
														</tr>
													</thead>
													<tbody>
														<tr>
															<td><a href="#!"
																target="_blank">
																<span><i
																		class="material-icons material-icons-outlined md-24">download_for_offline</i></span></a></td>
															
															<td>77/88/99</td>
															<td>-</td>
															<td>-</td>
															<td>A</td>
															<td>-</td>
															<td>-</td>
															<td>N/A</td>
														</tr>
													</tbody>
												</table>
												<p>" - " : conforme ou pas d'action, " A " : action requise, " N/A" : non applicable
													<br>
													Ceci est un exemple, car le point est à mettre en oeuvre de façon traçable...</p>
											</div>
										</div>
									</div>
									<?php } ?>
								</li>