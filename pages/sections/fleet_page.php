            <div class="section">
				<div class="container">
					<div class="row">
						<div class="col-12">
							<div class="section-heading heading-center section-heading-animate">
								<div class="section-subheading">les 5 avions de l'AéroClub de l'Ardèche - ACA</div>
								<h1>La Flotte</h1>
							</div>
						</div>
						<div class="col-lg-4 col-md-6 col-12 item">
							<!-- Begin news item -->
							<article class="news-item item-style">
								<a <?php if ($grinchLevel <= 1400) { ?>href="flotte-dr220-f-ch.php" <?php } ?> class="news-item-img el">
									<img data-src="../assets/img/ACA2403-FBPCH-4921.jpeg" class="lazy"
										src="data:image/gif;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII="
										alt="">
								</a>
								<div class="news-item-info">
									<div class="news-item-date">F-BPCH</div>
									<h2 class="news-item-heading item-heading">
										<a <?php if ($grinchLevel <= 1400) { ?> href="flotte-dr220-f-ch.php" <?php } ?> title="DR220 F-BPCH">DR220</a>
									</h2>
									<div class="news-item-desc">
										<p>Bois et toile, conçu et construit en France. Dédié à l'école</p>
									</div>
								</div>
							</article><!-- End news item -->
						</div>
						<div class="col-lg-4 col-md-6 col-12 item">
							<!-- Begin news item -->
							<article class="news-item item-style">
								<a <?php if ($grinchLevel <= 1400) { ?> href="flotte-pa18-f-es.php" <?php } ?> class="news-item-img el">
									<img data-src="../assets/img/ACA2109-FBFES.jpeg" class="lazy"
										src="data:image/gif;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII="
										alt="">
								</a>
								<div class="news-item-info">
									<div class="news-item-date">F-BFES</div>
									<h2 class="news-item-heading item-heading">
										<a <?php if ($grinchLevel <= 1400) { ?> href="flotte-pa18-f-es.php" <?php } ?> title="PA18 F-BFES">PA18</a>
									</h2>
									<div class="news-item-desc">
										<p>structure aluminium et toile, conçu et construit aux USA. Excellent en montagne</p>
									</div>
								</div>
							</article><!-- End news item -->
						</div>
                        <div class="col-lg-4 col-md-6 col-12 item">
							<!-- Begin news item -->
							<article class="news-item item-style">
								<a <?php if ($grinchLevel <= 1400) { ?> href="flotte-d140-f-bv.php" <?php } ?> class="news-item-img el">
									<img data-src="../assets/img/ACA2403-FGEBV.jpg" class="lazy"
										src="data:image/gif;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII="
										alt="">
								</a>
								<div class="news-item-info">
									<div class="news-item-date">F-GEBV</div>
									<h2 class="news-item-heading item-heading">
										<a <?php if ($grinchLevel <= 1400) { ?> href="flotte-d140-f-bv.php" <?php } ?> title="D140 F-GEBV">D140</a>
									</h2>
									<div class="news-item-desc">
										<p>Bois et toile, conçu et produit en France. Découverte, voyage et montagne.</p>
									</div>
								</div>
							</article><!-- End news item -->
						</div>
						<div class="col-lg-4 col-md-6 col-12 item">
							<!-- Begin news item -->
							<article class="news-item item-style">
								<a <?php if ($grinchLevel <= 1400) { ?> href="flotte-f172-f-ul.php" <?php } ?> class="news-item-img el">
									<img data-src="../assets/img/ACA2403-FBTUL.jpeg" class="lazy"
										src="data:image/gif;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII="
										alt="">
								</a>
								<div class="news-item-info">
									<div class="news-item-date">F-BTUL</div>
									<h2 class="news-item-heading item-heading">
										<a <?php if ($grinchLevel <= 1400) { ?> href="flotte-f172-f-ul.php" <?php } ?> title="F172L F-BTUL">F172L</a>
									</h2>
									<div class="news-item-desc">
										<p>Tout aluminium, conçu aux USA et construit en France. La bête de somme</p>
									</div>
								</div>
							</article><!-- End news item -->
						</div>
						<div class="col-lg-4 col-md-6 col-12 item">
							<!-- Begin news item -->
							<article class="news-item item-style">
								<a <?php if ($grinchLevel <= 1400) { ?> href="flotte-pa28-f-ef.php" <?php } ?> class="news-item-img el">
									<img src="../assets/img/ACA2403-FGKEF.jpeg" class="lazy"
                                    src="data:image/gif;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII="
                                    alt="">
								</a>
								<div class="news-item-info">
									<div class="news-item-date">F-GKEF</div>
									<h2 class="news-item-heading item-heading">
										<a <?php if ($grinchLevel <= 1400) { ?> href="flotte-pa28-f-ef.php" <?php } ?> title="PA28 F-GKEF">PA28</a>
									</h2>
									<div class="news-item-desc">
										<p>Tout aluminium, conçu et construit aux USA. Voyage, observation</p>
									</div>
								</div>
							</article><!-- End news item -->
						</div>						
					</div>
				</div>
			</div>

			<section>
            <div class="container">
                <div class="section-heading heading-center">
                    <div class="intro-content" style="--margin-left: 4rem;">
                        <div class="btn-group intro-btns">
		    				<div class="col">
                                S'il était convenable de vous proposer une alternative, la voici 😉 :
		    	                <a href="https://www.gorges-ardeche-pontdarc.fr/activite/les-gorges-de-lardeche-vues-du-ciel-labeaume/" target="_blank" class="btn btn-border btn-with-icon btn-small ripple">
	    			    	        <span>Vol découverte</span>
    					            <svg class="btn-icon-right" viewBox="0 0 13 9" width="13" height="9">
					    	            <use xlink:href="../assets/img/sprite.svg#arrow-right"></use>
				    	            </svg>
    			    	        </a>
                                ou encore, nous contacter pour...
		        		        <a href="../pages/contacts.php" data-title="connexion" class="btn btn-border btn-with-icon btn-small ripple">
	    	    		            <span>... une demande d'info</span>
    			    		        <svg class="btn-icon-right" viewBox="0 0 13 9" width="13" height="9">
					    	            <use xlink:href="../assets/img/sprite.svg#arrow-right"></use>
					                </svg>
				                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>