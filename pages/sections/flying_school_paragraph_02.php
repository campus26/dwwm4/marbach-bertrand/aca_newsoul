                                <!-- Formation théorique LAPL et PPL --> 
                                <div class="tabs-item content">
                                    <p>Le curriculum de la théorie est identique pour le LAPL et le PPL. Notre
                                        école vous propose le programme déclaré "Cépadues". Les thèmes suivants sont
                                        abordés : ce sont les modules du test théorique.<br>
                                        Celui-ci est acquis si 75% de réussite sont
                                        atteints pour chacun des modules.<br>
                                        La durée de la validité du théorique est de deux ans et le candidat a 18 mois
                                        poour passer l'ensemble des modules à compter de la prémière réussite à l'un
                                        d'eux<br>
                                        <style>
                                            th,
                                            td {
                                                text-align: center;
                                            }

                                            td:first-child {
                                                text-align: left;
                                            }

                                            tfoot tr {
                                                border-top: 1px solid darkgray;
                                            }
                                        </style>
                                    <table>
                                        <thead>
                                            <tr>
                                                <th>Module</th>
                                                <th>Code Aerogligli</th>
                                                <th>Nb questions</th>
                                                <th>"Cut"</th>
                                                <th>Durée (min)</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Réglementation / Droit Aérien</td>
                                                <td>REG</td>
                                                <td>20</td>
                                                <td>15</td>
                                                <td>30</td>
                                            </tr>
                                            <td>Météorologie</td>
                                            <td>MET</td>
                                            <td>20</td>
                                            <td>15</td>
                                            <td>30</td>
                                            </tr>
                                            <td> Performance humaine</td>
                                            <td>PHL</td>
                                            <td>12</td>
                                            <td>9</td>
                                            <td>18</td>
                                            </tr>
                                            <td>Communications</td>
                                            <td>COM</td>
                                            <td>8</td>
                                            <td>6</td>
                                            <td>12</td>
                                            </tr>
                                            <td>Principes du vol</td>
                                            <td>PDV</td>
                                            <td>12</td>
                                            <td>9</td>
                                            <td>18</td>
                                            </tr>
                                            <td>Procédures opérationnelles</td>
                                            <td>PRO</td>
                                            <td>12</td>
                                            <td>9</td>
                                            <td>18</td>
                                            </tr>
                                            <td>Performances et préparation du vol</td>
                                            <td>PER</td>
                                            <td>12</td>
                                            <td>9</td>
                                            <td>18</td>
                                            </tr>
                                            <td>Connaissance générale des aéronefs</td>
                                            <td>CGA</td>
                                            <td>12</td>
                                            <td>9</td>
                                            <td>18</td>
                                            </tr>
                                            <td>Navigation</td>
                                            </td>
                                            <td>NAV</td>
                                            <td>20</td>
                                            <td>15</td>
                                            <td>50</td>
                                            </tr>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td colspan="2"></td>
                                                <td>128</td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                    La table des matières de la progression du "Manuel du Pilote" se consulte ici :
                                    <br><a
                                        href="<?php echo $extStoragePath; ?>aca-pdf/ACA2403-DTO-Cepadues_190100-table-des-matieres.pdf"
                                        target="_blank">
                                        <span><i
                                                class="material-icons material-icons-outlined md-48">download_for_offline</i></span></a><br><br>
                                    Votre progression théorique sera autonome et se fera à l'aide du site aerogligli.
                                    <br>
                                    Il est recommandé de s'inscrire à l'examen théorique lorsque le score à
                                    l'entraînement dépasse régulièrement 85% de bonnes réponses <strong>...et...</strong> lorsque les navigations auront débuté.
                                    <br>
                                    <?php if ($grinchLevel <= 1400) { ?>
                                    Pour souscrire au suivi aerogligli et à l'entrainement aux QCM, c'est par ici
                                    :<br><a
                                        href="<?php echo $extStoragePath; ?>aca-pdf/ACA2403-DTO-e-learning Aerogligli_LAPL_PPL-inscription.pdf"
                                        target="_blank">
                                        <span><i
                                                class="material-icons material-icons-outlined md-48">download_for_offline</i></span></a>
                                    <?php } ?>
                                    <br><br>

                                    <mark>Bien entendu, les instructeurs de l'équipe pédagogique sont à votre
                                        disposition sur demande. Sollicitez-les !...</mark></p>
                                </div>