                                    <!-- Conversion LAPL vers PPL -->
                                    <div class="tabs-item content">
                                    <p>Le programme de conversion de la LAPL vers la PPL va demander 10h de vol de
                                        double complémentaires.<br>
                                        c'est pourquoi il convient de bien réfléchir à l'opportunité de choisir de
                                        passer initiallement une LAPL
                                        plutôt qu'une PPL. En faisant l'hypothèse -très peu probable- d'une réussite au
                                        minimum des heures, pour
                                        obtenir la PPL, il faudra 30+10+10=50h de formation à privilèges égaux.<br>
                                        De plus quand il est rappelé que le minimum règlementaire de formation n'est
                                        quasiment jamais atteint, il devient
                                        aisé de comprendre pourquoi nous recommandons dès le départ une formation vers
                                        la LAPL. <br><br>
                                        Ce préambule énoncé, le programme de conversion qui est déclaré auprès de la
                                        DGAC
                                        est celui de l'ANPI.<br><br>
                                        Le programme est le suivant :</p>
                                    <ul>
                                        <strong>Conversion LAPL vers PPL</strong>
                                        <li><a href="<?php echo $extStoragePath; ?>aca-pdf/ACA2403-DTO-ANPI-Livret-formation-LAPLA-vers-PPLA-DTO-REV-1-Version-1-01-Fevrier-2023.pdf"
                                                target="_blank">
                                                <span><i
                                                        class="material-icons material-icons-outlined md-48">download_for_offline</i></span></a>⇥ Livret
                                            de Formation</li>
                                        <li><a href="<?php echo $extStoragePath; ?>aca-pdf/ACA2403-DTO-ANPI-Livret-stagiaire-LAPLA-vers-PPLA-DTO-REV-1-Version-1-01-Fevrier-2023.pdf"
                                                target="_blank">
                                                <span><i
                                                        class="material-icons material-icons-outlined md-48">download_for_offline</i></span></a>⇥ Livret
                                            de progression</li>
                                    </ul>
                                </div>