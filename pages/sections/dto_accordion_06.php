<li class="accordion-item section-bgc">
									<div class="accordion-trigger">
										<div><a href="#!" target="_blank" title="" style="display: inline-block;">
												<img src="../assets/img/ACA2403-Icone-chercher.png"
													alt="icone d'incident et d'alerte'" width="44" height="44">
											</a>∾ CRESAG
										</div>
									</div>
									<?php if ($grinchLevel <= 1013) { ?>
									<div class="accordion">
										<div class="row gutters-default">
											<div class="accordion-content content">
												<table class="table-secondary">
													<thead>
														<tr>
															<th>CRESAG</th>
															<th>référence</th>
															<th>Date</th>
															<th>Titre</th>
															<th>Déposé</th>
															<th>Analysé</th>
															<th>Correctives</th>
															<th>Destinataires</th>
															<th>Publié</th>
														</tr>
													</thead>
													<tbody>
														<tr>
															<td><a href="../assets/media/zip/ACA2403-CRESAG24001.zip"
																target="_blank">
																<span><i
																		class="material-icons material-icons-outlined md-24">download_for_offline</i></span></a></td>
															
															<td>ACA24001</td>
															<td>02/02/24</td>
															<td>Arrêt moteur en vol</td>
															<td>08/02/24</td>
															<td>16/03/24</td>
															<td>16/03/24</td>
															<td>BIA<br>DSAC<br>DSNA</td>
															<td>19/03/24</td>
														</tr>
														<tr>													
															<td><a href="<?php echo $extStoragePath; ?>aca-pdf/ACA2403-CRESAG23001.pdf"
																target="_blank">
																<span><i
																		class="material-icons material-icons-outlined md-24">download_for_offline</i></span></a></td>
															
															<td>23001</td>
															<td>28/10/23</td>
															<td>Intrusion CTR LFTW</td>
															<td>OK</td>
															<td>-</td>
															<td>-</td>
															<td>DSAC<br>DSNA<br>BIA</td>
															<td>-</td>
														</tr>
													</tbody>
												</table>
											</div>
										</div>
									</div>
									<?php } ?>
								</li>