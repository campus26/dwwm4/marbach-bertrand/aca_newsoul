<div class="section">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                        <div class="section-heading heading-center section-heading-animate">
                                <div class="section-subheading">Service indisponible</div>
                                <h1>503</h1>
                                <p class="section-desc">Le service requis n'est pas disponible (Error 503 Service unavailable).</p>
                            </div>
                            <div class="btn-group align-items-center justify-content-center">
                                <a href="<?php echo __DIR__ ?>/index.php" class="btn btn-border">
                                    <span>Retour à l'acueil</span>
                                    <svg class="btn-icon-right" viewBox="0 0 13 9" width="13" height="9">
                                        <use xlink:href="../assets/img/sprite.svg#arrow-right"></use>
                                    </svg>
                                </a>
                                <a href="<?php echo __DIR__ ?>/contacts.php" class="btn btn-border">
                                    <span>Laissez-nous un message</span>
                                    <svg class="btn-icon-right" viewBox="0 0 13 9" width="13" height="9">
                                        <use xlink:href="../assets/img/sprite.svg#arrow-right"></use>
                                    </svg>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>