                                 <!-- Qualification Vol de Nuit -->
                                 <div class="tabs-item content">
                                    <p>La formation VFR Nuit est certes un pré-requis vers le CPL et/ou l'IFR.<br>
                                        C'est également un excellent moyen de progresser vers un meilleur pilotage. Et
                                        si le pilotage est meilleur, la
                                        sécurité des vols sera améliorée.<br>
                                        De plus le vol de nuit reste emprunt d'un petit mystère, d'une grande magie et
                                        c'est
                                        aussi pour cela que nous souhaitons promouvoir
                                        -en sécurité- cette façon de piloter, plus rigoureuse, mais aussi tout à fait
                                        gratifiante.<br>
                                        <br>
                                    <h5>Formation Vol de Nuit</h5>
                                    <a href="<?php echo $extStoragePath; ?>aca-pdf/ACA2403-DTO-ANPI-VDN-Livret-formation-V8.pdf"
                                        target="_blank">
                                        <span><i
                                                class="material-icons material-icons-outlined md-48">download_for_offline</i></span></a>⇥ Livret
                                    de formation au VFR Nuit<br>
                                    <a href="<?php echo $extStoragePath; ?>aca-pdf/ACA2403-DTO-ANPI-VDN-Livret-stagiaire-REV-0-Version-1-14-Novembre-2018.pdf"
                                        target="_blank">
                                        <span><i
                                                class="material-icons material-icons-outlined md-48">download_for_offline</i></span></a>⇥ Livret
                                    de progression au VFR nuit<br>
                                    <br>
                                    <?php if ($grinchLevel <= 1400) { ?>
                                    <h5>Ressource de cours complémentaire</h5>
                                    <a href="<?php echo $extStoragePath; ?>aca-pdf/ACA2403-DTO-VDN-Plan-Formation-Pratique.pdf"
                                        target="_blank">
                                        <span><i
                                                class="material-icons material-icons-outlined md-48">download_for_offline</i></span></a>⇥ exposé
                                    des phases de progression<br>
                                    <a href="<?php echo $extStoragePath; ?>aca-pdf/ACA2403-VDN_MasterTable.pdf"
                                        target="_blank">
                                        <span><i
                                                class="material-icons material-icons-outlined md-48">download_for_offline</i></span></a>⇥ VFR
                                    Nuit : "fly by numbers" ou préparez votre tableau de pré-affichages<br>
                                    <a href="<?php echo $extStoragePath; ?>aca-ppt/ACA2403-DTO-VDN-Vol-de-nuit.pptx"
                                        target="_blank">
                                        <span><i
                                                class="material-icons material-icons-outlined md-48">download_for_offline</i></span></a>⇥ VFR
                                    Nuit au format PowerPoint<br>
                                    <a href="<?php echo $extStoragePath; ?>aca-key/ACA2403-DTO-VDN-Vol-de-nuit-.key"
                                        target="_blank">
                                        <span><i
                                                class="material-icons material-icons-outlined md-48">download_for_offline</i></span></a>⇥ VFR
                                    Nuit au format Keynote<br>
                                    <?php } ?>

                                    <div class="image-container">
                                        <img src="../assets/img/ACA2403-DTO-VDN-IMG_2403.jpeg"
                                            alt="Le vol de nuit, c'est chouette !">
                                    </div>
                                    </p>
                                </div>