<div class="section">
                <div class="container">
                    <div class="row content-items">
                        <div class="col-12">
                            <div class="section-heading">
                                <div class="section-subheading">Tous les moyens sont bons pour nous joindre / We are
                                    accessible through various means</div>
                                <h1>Contacts</h1>
                            </div>
                        </div>
                        <div class="col-xl-4 col-md-5 content-item">
                            <?php include("../pages/templates/our_details.php"); ?>
                        </div>
                        <div class="col-xl-8 col-md-7 content-item">
                            <?php include("../pages/templates/contact_form.php"); ?>
                        </div>
                    </div>
                </div>
            </div>