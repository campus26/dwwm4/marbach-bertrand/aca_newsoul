<!-- Pilot stories or Aviator Narratives or Pilot Experiences or Flying Adventures or Airman Tales or Pilot Insights -->
<section class="section section-bgc">
				<div class="container">
					<div class="row items">
						<header class="col-12">
							<div class="section-heading heading-center">
								<div class="section-subheading">Nos histoires et nos vols : on partage ! </div>
								<h2>Voyagez avec nous</h2>
							</div>
						</header>
						<div class="col-lg-4 col-md-6 col-12 item">
							<article class="news-item item-style">
								<a href="#!" class="news-item-img el">
									<img data-src="../assets/img/ACA24032023-08-01.jpg" class="lazy" src="data:image/gif;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII=" alt="">
								</a>
								<div class="news-item-info">
									<div class="news-item-date">Ruoms-Brest</div>
									<h3 class="news-item-heading item-heading">
										<a href="#!" title="Benefits Of Async/Await">Vers la bretagne</a>
									</h3>
									<div class="news-item-desc">
										<p>Deux pilotes sont allés trouver le trait côtier et l'ont remonté jusqu'à Brest.</p>
									</div>
								</div>
							</article>
						</div>
						<div class="col-lg-4 col-md-6 col-12 item">
							<article class="news-item item-style">
								<a href="#!" class="news-item-img el">
									<img data-src="../assets/img/ACA2403IMG_20200826_110814.jpg" class="lazy" src="data:image/gif;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII=" alt="">
								</a>
								<div class="news-item-info">
									<div class="news-item-date">Ruoms - Propriano</div>
									<h3 class="news-item-heading item-heading">
										<a href="#!" title="Key Considerations Of IPaaS">Voler, Atterrir, Plage !</a>
									</h3>
									<div class="news-item-desc">
										<p>La Corse, c'est à côté ! 2h30 de vol depuis Ruoms</p>
									</div>
								</div>
							</article>
						</div>
						<div class="col-lg-4 col-md-6 col-12 item">
							<article class="news-item item-style">
								<a href="#!" class="news-item-img el">
									<img data-src="../assets/img/ACA2403DSC02160.jpg" class="lazy" src="data:image/gif;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII=" alt="">
								</a>
								<div class="news-item-info">
									<div class="news-item-date">Vol local</div>
									<h3 class="news-item-heading item-heading">
										<a href="#!" title="Hibernate Query Language">Montagne adéchoise</a>
									</h3>
									<div class="news-item-desc">
										<p>Un peu plus loin que les célèbres gorges du vol local, mais pas trop : la montagne et
											le plateau ardéchois gardent de beaux secrets ! </p>
									</div>
								</div>
							</article>
						</div>
						<footer class="section-footer col-12 item section-footer-animate">
							<div class="btn-group align-items-center justify-content-center">
								<a href="#!" class="btn btn-with-icon btn-w240 ripple">
									<span>Lisez, régalez-vous !</span>
									<svg class="btn-icon-right" viewBox="0 0 13 9" width="13" height="9">
										<use xlink:href="../assets/img/sprite.svg#arrow-right"></use>
									</svg>
								</a>
							</div>
						</footer>
					</div>
				</div>
			</section>