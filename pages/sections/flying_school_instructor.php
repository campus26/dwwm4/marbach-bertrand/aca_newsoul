<article class="section">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="section-heading heading-center section-heading-animate">
                                <h1>Corporate Solutions</h1>
                            </div>
                            <div class="col-12 item">
                                <div class="row items">
                                    <div class="content">
                                        <div class="item-bordered item-border-radius">
                                            <img src="../assets/img/subheader-corporate.png" alt="">
                                        </div>
                                    </div>
                                </div>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque velit nisi, pretium
                                    ut lacinia in, elementum id enim. Curabitur aliquet quam id dui posuere blandit.
                                    Vivamus suscipit tortor eget felis porttitor volutpat. Curabitur non nulla sit amet
                                    nisl tempus convallis quis ac lectus. Lorem ipsum dolor sit amet, consectetur
                                    adipiscing elit. Quisque velit nisi, pretium ut lacinia in, elementum id enim.
                                    Curabitur aliquet quam id dui posuere blandit. Vivamus suscipit tortor eget felis
                                    porttitor volutpat.</p>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque velit nisi, pretium
                                    ut lacinia in, elementum id enim. Curabitur aliquet quam id dui posuere blandit.
                                    Vivamus suscipit tortor eget felis porttitor volutpat. Curabitur non nulla sit amet
                                    nisl tempus convallis quis ac lectus.</p>
                                <h5>Key Features</h5>
                                <ul>
                                    <li>High usability</li>
                                    <li>Improved performance</li>
                                    <li>Customizable interface</li>
                                    <li>Crossplatform support</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </article>

            <section class="section section-bgc">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="section-heading heading-center section-heading-animate">
                                <div class="section-subheading">Liked the service?</div>
                                <h2>Order service</h2>
                            </div>
                            <form action="#!" method="post" class="order-form">
                                <!-- Begin hidden Field for send form -->
                                <input type="hidden" name="Subject" value="Order service">
                                <!-- End hidden Field for send form -->
                                <div class="row gutters-20 justify-content-center">
                                    <div class="col-lg-3 col-md-4 col-sm-6 col-12">
                                        <div class="form-field">
                                            <label for="order-name" class="form-field-label">Your Name</label>
                                            <input type="text" class="form-field-input" name="orderName" value=""
                                                autocomplete="off" id="order-name">
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-4 col-sm-6 col-12">
                                        <div class="form-field">
                                            <label for="order-phone" class="form-field-label">Your Phone</label>
                                            <input type="tel" class="form-field-input mask-phone" name="orderPhone"
                                                value="" autocomplete="off" id="order-phone">
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-4 col-12">
                                        <div class="form-btn form-btn-wide">
                                            <button type="submit"
                                                class="btn btn-w240 ripple"><span>Order</span></button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </section>