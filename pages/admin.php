<?php
include("init/config-pages.php");
include("../assets/functions/functionsDB.php");

$title_page = $entity . " - page admin ";

$breadcrumbs = array(
    array("url" => "../pages/index-landing-page.php", "title" => "Accueil"),
);

include("templates/header.php");
// if ($grinchLevel == 0) {
    include("sections/admin_page.php");
// } else {
//     echo "<br>";
//     echo "<h4 style='text-align: center;'>Désolé...<br> ...mais vous n'avez pas les droits pour accéder à cette page 😉</h4>";
// }
include("templates/footer.php");
include("templates/include_js_scripts.php");
?>