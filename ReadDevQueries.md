```
### #################################################################
###   Best way to remember dev queries and be sure to find them  ###
###      is to store them here. And remember they are here...    ###
### #################################################################
```
### Friday 26/04/2024
```
ways of passing variable from one page to another
```

1. **Using Sessions**: You can store variables in session data, which persists across multiple page loads until the session is destroyed. Here's an example:

    On the first page:
    ```php
    <?php
    session_start();
    $_SESSION['variable1'] = $value1;
    $_SESSION['variable2'] = $value2;
    // Redirect to another page
    header('Location: nextpage.php');
    exit;
    ?>
    ```

    On the second page:
    ```php
    <?php
    session_start();
    $value1 = $_SESSION['variable1'];
    $value2 = $_SESSION['variable2'];
    // Use $value1 and $value2
    ?>
    ```

2. **Using Cookies**: You can store variables in cookies, which are saved on the user's computer and sent with each request. Here's an example:

    On the first page:
    ```php
    <?php
    setcookie("variable1", $value1, time() + (86400 * 30), "/"); // 86400 = 1 day
    setcookie("variable2", $value2, time() + (86400 * 30), "/");
    // Redirect to another page
    header('Location: nextpage.php');
    exit;
    ?>
    ```

    On the second page:
    ```php
    <?php
    $value1 = $_COOKIE['variable1'];
    $value2 = $_COOKIE['variable2'];
    // Use $value1 and $value2
    ?>
    ```

3. **Using GET or POST Parameters**: You can pass variables through URLs using GET parameters or through form submissions using POST parameters. Here's an example using GET parameters:

    On the first page:
    ```php
    <?php
    // Redirect to another page with parameters in the URL
    header('Location: nextpage.php?variable1=' . urlencode($value1) . '&variable2=' . urlencode($value2));
    exit;
    ?>
    ```

    On the second page:
    ```php
    <?php
    $value1 = $_GET['variable1'];
    $value2 = $_GET['variable2'];
    // Use $value1 and $value2
    ?>
    ```

4. **Using Local Storage**:
    - Local storage allows you to store key-value pairs in the user's web browser, which persists even after the browser is closed.
    
    On the first page:
    ```javascript
    // Store variables in local storage
    localStorage.setItem('variable1', value1);
    localStorage.setItem('variable2', value2);
    // Redirect to another page
    window.location.href = 'nextpage.php';
    ```

    On the second page:
    ```javascript
    // Retrieve variables from local storage
    var value1 = localStorage.getItem('variable1');
    var value2 = localStorage.getItem('variable2');
    // Use value1 and value2
    ```

When using local storage, it's important to remember that the data is stored on the user's device and is accessible to JavaScript code running in the same origin. Also, local storage has limitations on the amount of data that can be stored and does not expire automatically. Therefore, it's important to handle data securely and consider user privacy implications.
    ```

4. **Using AJAX**:
    - there is also AJAX which allows to exchange varialbes between JS and PHP. Not that easy to implement. Not yet tried here.
    https://www.quora.com/How-do-I-send-a-JavaScript-variable-to-PHP-variable

    ```
Each of these methods has its own use case and considerations regarding security, scalability, and user experience. Choose the one that best fits your requirements.
