<?php
require( __DIR__ . '/DataBase.php');

class Auth
{

    private $db;
    public function __construct()
    {
        $this->db = new DataBase();
    }
    public function checkLogin($email, $password)
    {
        $password = hash('sha256', $password);
    
        $sql = "SELECT id, entity, name_first, name_last, ext_storage_path, grinch_level, session_time_out, profile_picture_url, time_start_actual_session 
            FROM users 
            WHERE email = ? AND password = ?";
        $stmt = $this->db->getConn()->prepare($sql);
        $stmt->bind_param("ss", $email, $password);
        $stmt->execute();
        $result = $stmt->get_result();
    
        if ($result->num_rows == 1) {
            $row = $result->fetch_assoc();
    
            $timeNow = date("Y-m-d H:i:s");
            $updateQuery = "UPDATE users SET time_start_actual_session = '$timeNow' WHERE id = {$row['id']}";
            $this->db->getConn()->query($updateQuery);
    
            $response = [
                'valid' => true,
                'userID' => $row['id'],
                'entity' => $row['entity'],
                'userNameFirst' => $row['name_first'],
                'userNameLast' => $row['name_last'],
                'extStoragePath' => $row['ext_storage_path'],
                'grinchLevel' => $row['grinch_level'],
                'sessionTimeOut' => $row['session_time_out'],
                'profilePictureUrl' => $row['profile_picture_url'],
                'timeStartActualSession' => $row['time_start_actual_session'],
            ];
        } else {
            $response = ['valid' => false];
        }
        $stmt->close();
        $this->db->getConn()->close();
        return $response;
    }
    
}
