<?php
require(__DIR__ . './../../class/Auth.php');

$auth = new Auth();

$email = $_POST['email'] ?? '';
$password = $_POST['password'] ?? '';

$rep =  $auth->checkLogin($email, $password);


header('Content-Type: application/json');
echo json_encode($rep);
