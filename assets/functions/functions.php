<?php
//grinch : Golden Rigorous Internal Network Clearance Hierarchy 

// G: Golden            (level 0 :    0) • sessionTimeOut :    no • *** utilsé pour le développement ***)
// R: Reserved          (level 1 :  353) • sessionTimeOut : 1800s • Président,Secrétaire, Trésorier, Mécanicien, SGS, RP)
// I: Illustrious       (level 2 : 1013) • sessionTimeOut : 1800s • externe, DSAC)
// N: Notable access    (level 3 : 1031) • sessionTimeOut : 1800s • membres du comité directeur, instructeurs)
// C: Conventional      (level 4 : 1400) • sessionTimeOut : 1800s • adhérents de l'association sans fonctions électives)
// H: Humble access     (level 5 : 7600) • sessionTimeOut : 1800s • visiteurs, non adhérents sans accès spécifique)

function setUserPrivilege($grinch_status): int
{
    $privilege = 7600;

    switch ($grinch_status) {
        case 'G':
            $privilege = 0;
            break;
        case 'R':
            $privilege = 353;
            break;
        case 'I':
            $privilege = 1013;
            break;
        case 'N':
            $privilege = 1031;
            break;
        case 'C':
            $privilege = 1400;
            break;
        case 'H':
            $privilege = 7600;
            break;
    }

    return $privilege;
}

function isSessionDeprecated($startTime, $actualTime, $sessionTimeout)
{
    $startUnixTime = strtotime($startTime);
    $actualUnixTime = strtotime($actualTime);

    $elapsedTime = $actualUnixTime - $startUnixTime;

    if ($elapsedTime > $sessionTimeout && $sessionTimeout != 0) {
        return true;
    } else {
        return false;
    }
}