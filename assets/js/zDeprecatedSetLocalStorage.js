const CryptoJS = require('crypto-js'); 

function encrypt(textString) {
    hash=textString;
    // const hash = CryptoJS.SHA256(textString).toString(CryptoJS.enc.Hex);
    return hash;
}

// Initial user data - In the future, this will be retrieved from a databas
function setLocalStorage () {
    const userProfiles = [
        {
            user_ID: 0,
            user_password: encrypt("ViveLeDev!"),
            user_first_name: "Bertrand",
            user_name: "MARBACH",
            user_email: "bertrand.marbach@oumpahpah.fr",
            user_mobile_phone: "+33.6.519.777.46",
            user_grinch_level: "G",
            user_session_time_out : 0,
            user_start_connexion: formatDate(new Date()),
            user_has_acknowledged_terms_condition: true,
            user_profile_picture_url: "../assets/img/icons/statusIsConnected.png",
            entity: "ACA",
            ext_storage_path: "https://storage.marbach-bertrand.ovh/"
        },
        {
            user_ID: 1,
            user_password: encrypt("EscargotMusicalPlanant$"),
            user_first_name: "Jacqueline",
            user_name: "COCHRAN",
            user_email: "bureau@autremail.aero",
            user_mobile_phone: "+33.6.80.84.72.97",
            user_grinch_level: "R",
            user_session_time_out : 1800,
            user_start_connexion: formatDate(new Date()),
            user_has_acknowledged_terms_condition: true,
            user_profile_picture_url: "../assets/img/icons/statusIsConnected.png",
            entity: "ACA",
            ext_storage_path: "https://storage.marbach-bertrand.ovh/"
        },
        {
            user_ID: 2,
            user_password: encrypt("PélicanPaisible&Polarisé"),
            user_first_name: "Hélène",
            user_name: "BOUCHER",
            user_email: "organisme-formation-pn.dsac-ce@aviation-civile.gouv.fr",
            user_mobile_phone: "+33.6.80.84.72.97",
            user_grinch_level: "I",
            user_session_time_out : 1800,
            user_start_connexion: formatDate(new Date()),
            user_has_acknowledged_terms_condition: true,
            user_profile_picture_url: "../assets/img/icons/statusIsConnected.png",
            entity: "ACA",
            ext_storage_path: "https://storage.marbach-bertrand.ovh/"
        },
        {
            user_ID: 3,
            user_password: encrypt("Crevett3VoltigeanteEnjouée"),
            user_first_name: "Bob",
            user_name: "HOOVER",
            user_email: "comiteLFHF@autremail.aero",
            user_mobile_phone: "+33.6.80.84.72.97",
            user_grinch_level: "N",
            user_session_time_out : 1800,
            user_start_connexion: formatDate(new Date()),
            user_has_acknowledged_terms_condition: true,
            user_profile_picture_url: "../assets/img/icons/statusIsConnected.png",
            entity: "ACA",
            ext_storage_path: "https://storage.marbach-bertrand.ovh/"
        },
        {
            user_ID: 4,
            user_password: encrypt("CroissantAcrobatiqueDegourdi!"),
            user_first_name: "Bessie",
            user_name: "COLEMAN",
            user_email: "contact@aeroclubdelardeche.com",
            user_mobile_phone: "+33.6.80.84.72.97",
            user_grinch_level: "C",
            user_session_time_out : 1800,
            user_start_connexion: formatDate(new Date()),
            user_has_acknowledged_terms_condition: true,
            user_profile_picture_url: "../assets/img/icons/statusIsConnected.png",
            entity: "ACA",
            ext_storage_path: "https://storage.marbach-bertrand.ovh/"
        },
        {
            user_ID: 5,
            user_password: "h3liceJuste&Parfaite",
            user_first_name: "Jacqueline",
            user_name: "AURIOL",
            user_email: "contact@autremail.tdp",
            user_mobile_phone: "+33.6.80.84.72.97",
            user_grinch_level: "H",
            user_session_time_out : 1800,
            user_start_connexion: formatDate(new Date()),
            user_has_acknowledged_terms_condition: true,
            user_profile_picture_url: "../assets/img/icons/statusIsConnected.png",
            entity: "ACA",
            ext_storage_path: "https://storage.marbach-bertrand.ovh/"
        }
    ];
    
    if (localStorage.length > 0) {
        localStorage.clear();
        console.log("Local storage cleared successfully.");
    } else {
        console.log("Local storage is not used.");
    }

    localStorage.setItem('userProfiles', JSON.stringify(userProfiles));

    const retrievedUserProfiles = JSON.parse(localStorage.getItem('userProfiles'));
    console.log(retrievedUserProfiles);
}

function formatDate(date) {
    const year = date.getUTCFullYear();
    const month = String(date.getUTCMonth() + 1).padStart(2, '0');
    const day = String(date.getUTCDate()).padStart(2, '0');
    const hours = String(date.getUTCHours()).padStart(2, '0');
    const minutes = String(date.getUTCMinutes()).padStart(2, '0');
    const seconds = String(date.getUTCSeconds()).padStart(2, '0');
    
    return `${year}-${month}-${day} ${hours}:${minutes}:${seconds}`;
}

setLocalStorage();