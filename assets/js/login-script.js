const navbarMenu = document.querySelector(".navbar .links");
const hamburgerBtn = document.querySelector(".hamburger-btn");
const hideMenuBtn = navbarMenu.querySelector(".close-btn");
const showPopupBtn = document.querySelector(".login-btn");
const formPopup = document.querySelector(".form-popup");
const hidePopupBtn = formPopup.querySelector(".close-btn");
const bottomLinks = document.querySelectorAll(".bottom-link a");
const formBoxes = document.querySelectorAll(".form-box");
const loginBtn = document.querySelector(".login-input-btn");
const emailInput = document.querySelector('.email-input-field input[type="email"]');
const passwordInput = document.querySelector('.password-input-field input[type="password"]');

// Show mobile menu
hamburgerBtn.addEventListener("click", () => {
    navbarMenu.classList.toggle("show-menu");
});

// Hide mobile menu 
hideMenuBtn.addEventListener("click", () => hamburgerBtn.click());

// Show login popup
showPopupBtn.addEventListener("click", () => {
    document.body.classList.toggle("show-popup");
});
// Perform login when the user clicks the submit button
loginBtn.addEventListener("click", async (event) => {
    event.preventDefault();

    const email = emailInput.value.trim();
    const password = passwordInput.value.trim();

    const formData = new FormData();
    formData.append("email", email);
    formData.append("password", password);

    try {
        const response = await fetch("../assets/functions/validate_credentials.php", {
            method: "POST",
            body: formData,
        });

        if (!response.ok) {
            throw new Error("Erreur de serveur");
        }

        const data = await response.json();

        if (data.valid) {
            const { userID, entity, userNameFirst, userNameLast, extStoragePath, grinchLevel, sessionTimeOut, profilePictureUrl, timeStartActualSession } = data;

            // Redirect to index.php with additional user data
            window.location.href = `../index.php?userID=${userID}&entity=${entity}&userNameFirst=${userNameFirst}&userNameLast=${userNameLast}&extStoragePath=${extStoragePath}&grinchLevel=${grinchLevel}&sessionTimeOut=${sessionTimeOut}&profilePictureUrl=${profilePictureUrl}&timeStartActualSession=${timeStartActualSession}`;
        } else {
            console.log("droits d'accès non conformes");
            // Display error message if needed
        }
    } catch (error) {
        console.error("Erreur : ", error.message);
        // Display error message if needed
    }
});

// Trigger login popup on page load
document.addEventListener("DOMContentLoaded", function () {
    const form = document.querySelector('form');
    const emailInputField = form.querySelector('.email-input-field');

    // Automatically triggers the login popup on page load
    showPopupBtn.click();

});

// Hide login popup and redirect to index.php
hidePopupBtn.addEventListener("click", () => {
    document.body.classList.remove("show-popup");
    window.location.href = "../pages/index-landing-page.php";
});

// Show or hide form boxes based on bottom link clicks
bottomLinks.forEach(link => {
    link.addEventListener("click", (e) => {
        e.preventDefault();
        
        // Get the ID of the target form box to show
        const targetFormId = link.dataset.target;
        
        // Hide all form boxes
        formBoxes.forEach(box => {
            box.style.display = "none";
        });
        
        // Show the target form box
        const targetFormBox = document.getElementById(targetFormId);
        targetFormBox.style.display = "flex";
    });
});