function playVideo(event) {
    event.preventDefault(); // Prevent the default action of the anchor tag
    var video = event.target.closest('.news-item').querySelector('video');
    if (video) {
        video.play(); // Play the video
    }
}