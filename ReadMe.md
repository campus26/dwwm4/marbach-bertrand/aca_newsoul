```
### #################################################################
### remember to always do the following before pushing/syncing : ###
###                        rm -f .DS_Store                       ###
### #################################################################
```

### Mo 22/04/2024
```
Finished migration to php in order to implement access privileges
```
### SU 21/04/2024
```
Started migration to php in order to implement access privileges
```
### Fr 19/04/2024
```
Went live with the user form. used free API from https://web3forms.com/
Implemented user form
Added social logos
```

### Th 18/04/2024
```
Started to work on remaining functionalities not yet implemented :
   • Set up a user form. Forwarding the data to the organisation's email.
   • Create an Entity-Relationship Diagram (ERD) and set up a DB to allow identified users to download certain files (general meeting reports, etc.)
   • Create a restricted access area. Depending on the access privileges, different parts of the site are/not restricted
```
### Sa 23/03/2024
```
Debugigng inputs from Christine T.
```

```
'private' deployment done.
Public temporary page still visible under index.html. no links
'private' site accessible with www.areoclubdelardeche.com/indexTest.html. all links active, click on 'accueil' leads to temporary public page
```

### Lu 11/03/2024 
```
Old menus vs new menus

|   old menu   |     new menu    |
|:------------:|:---------------:|
| Home         | accueil         |
| About us     | a-propos        |
| News         | flotte          |
| Services     | ecole           |
| Contacts     | contacts        |
| Tabs and...  | Prépa Vol & Nav |
| Terms and... | infos-legales   |
| Privacy P... | a-definir       |
```

### Sa 09/03/2024 

```
Overview
    Mid december '23 DTO (Declared Training Organisation) ACA had its wp site destroyed through attacks since it was not updated anymore for quite a bunch of time. 
    The French Civilian Aviation Authority advised a couple of weeks ago its intention to conduct the organisation's audit on March, 21st.
    We need urgently to restore a minimalist site in which it will be possible for student pilots and licenced pilots to find again all safety, regulatory, and administrative information and tools which were available before the hack.
```
```
We found a free html/css theme and now it is time to built some pages again.

