```
```
## Change Log version Updates:


- Done : Established distinct user credentials based on roles.
- Done : Planned implementation of complementary database still needs to be realised. For example, transitioning from LocalStorage to a properly secured database for authorized users.
- MCD/MLD still needs to be improvemnt, for instance demonstrate intermediate tables.
- see issue board for further https://gitlab.com/campus26/dwwm4/marbach-bertrand/aca_newsoul/-/boards


## v1.09 - Sa 11/05/2024

Backstage improvment
- various minor adjustments
- changed misspelling of userID variable in parameter exchange function


## v1.08 - Th 09/05/2024

Backstage improvment
- sessions are now tracable per user 
- visited pages are now tracable
- some minor adjustments


## v1.07 - Th 09/05/2024

Backstage improvment
- DB management : added information of last login for session tracking.
- Reset user DB
- various minor adjustments, including some debugging (variable incorrectly named with a capital letter)


## v1.06 - We 08/05/2024

Backstage réorganisation
- changes variable names in a more self-explanatory and structured way


## v1.05 - Tu 07/05/2024

Backstage improvment
- DB management moved to object oriented programming (OOP)
- various minor adjustments


## v1.04 - Tu/07/05/2024

Login via DB functional
- added login routines
- deleted local Storage routine
- deprecated JS and php files used for local Storage management


## v1.03 - Th 02/05/2024

Went live with the whole site
- user identification with session time out
- user credentials differs according role 


## v1.02 - Fr/19/04/2024

Went live with the user form. used free API from https://web3forms.com/
- Implemented user form
- Added social logos

## v1.01 - Th 18/04/2024

Inserted webcams in menu header

## v1.0 - Th 18/04/2024

Rolled out public site with limited features


## v0.1 - Sa 23/03/2024

Debugigng inputs from Christine


Here is the public production site of our AéroClub de l'Ardèche wonderful site.
